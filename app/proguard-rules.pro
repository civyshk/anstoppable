# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

#-dontobfuscate
#
#-keepnames class ** { *; }
#-keepnames interface ** { *; }
#-keepnames enum ** { *; }

#-dontwarn android.support.v7.**
#-keep class android.support.v7.** { *; }
#-keep interface android.support.v7.** { *; }
#
#-dontwarn android.support.appcompat.v7.**
#-keep class android.support.appcompat.v7.** { *; }
#-keep interface android.support.appcompat.v7.** { *; }
#
#-dontwarn android.support.constraint.constraint.v7.**
#-keep class android.support.constraint.constraint.v7.** { *; }
#-keep interface android.support.constraint.constraint.v7.** { *; }
#
#-dontwarn android.support.recyclerview.v7.**
#-keep class android.support.recyclerview.v7.** { *; }
#-keep interface android.support.recyclerview.v7.** { *; }
#
#-dontwarn android.support.preference.v7.**
#-keep class android.support.preference.v7.** { *; }
#-keep interface android.support.preference.v7.** { *; }
#
#-dontwarn android.support.support.v4.**
#-keep class android.support.support.v4.** { *; }
#-keep interface android.support.support.v4.** { *; }

# #   implementation 'com.android.support:appcompat-v7:27.1.1'
# #   implementation 'com.android.support.constraint:constraint-layout:1.1.2'
# #   implementation 'com.android.support:design:27.1.1'
# #   implementation 'com.android.support:support-v4:27.1.1'
# #   implementation 'com.android.support:recyclerview-v7:27.1.1'
# #   implementation 'com.android.support:preference-v7:27.1.1'

#-keep class android.support.design.widget.** { *; }
#-keep interface android.support.design.widget.** { *; }
#-dontwarn android.support.design.**
#-keep public class * extends android.support.design.widget.CoordinatorLayout$Behavior {
#    public <init>(android.content.Context, android.util.AttributeSet);
#}