/*
 * Copyright (c) 2018 by civyshk
 * civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.project104.anstoppable
/*
interface Task {
    var id: Int
}
*/

enum class TaskType(val asInt: Int) {
    COUNTER(0), STOPWATCH(1), COUNTDOWN(2);

    companion object {

        /** @return the corresponding enum instance, or null if none matches */
        operator fun get(i: Int): TaskType? = when (i) {
            0 -> COUNTER
            1 -> STOPWATCH
            2 -> COUNTDOWN
            else -> null
        }
    }
}