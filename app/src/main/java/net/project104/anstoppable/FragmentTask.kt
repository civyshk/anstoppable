/*
 * Copyright (c) 2018 by civyshk
 * civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.project104.anstoppable


import android.annotation.SuppressLint

import android.content.Context
import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.util.Log
import android.view.Gravity
import android.view.View

abstract class FragmentTask() : Fragment() {

    /** Every task has its own task Id.
     * -1 means no task is associated yet */
    var taskId: Long = -1L

    var fab: FloatingActionButton? = null

    /**
     * Fragment constructor arguments are better passed within
     * a Bundle. Use this to mimic that. Copy this constructor
     * into subclasses when needed
     */
    @SuppressLint("ValidFragment")
    constructor (taskId: Long) : this(){
        arguments = Bundle().apply {
            putLong("TaskId", taskId)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        logd("onCreate()")
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onResume() {
        fab = activity!!.findViewById(R.id.fab)
//        fab?.layoutParams = (fab?.layoutParams as CoordinatorLayout.LayoutParams).apply {
//            gravity = Gravity.BOTTOM or Gravity.RIGHT
//        }
        fab?.visibility = View.GONE

        super.onResume()
    }

    internal fun saveCurrentTaskInPreferences() {
        logd("saveCurrentTaskInPreferences()")
        val prefEditor = activity?.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE)?.edit()
        prefEditor?.putLong(PREF_LAST_TASK, taskId)
        prefEditor?.apply() ?: Log.d(getClassTag(this), "Saved task $taskId")
    }

}
