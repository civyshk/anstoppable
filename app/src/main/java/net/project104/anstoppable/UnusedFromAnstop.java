/*
 * Copyright (C) 2009-2010 by mj
 *   fakeacc.mj@gmail.com
 * Portions of this file Copyright (C) 2012 Jeremy Monin
 *   jeremy@nand.net
 * Copyright (c) 2018 by civyshk
 *   civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.project104.anstoppable;

import android.content.SharedPreferences;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.widget.Toast;

import net.project104.anstoppable.clock.AnstopDbAdapter;
import net.project104.anstoppable.clock.ThemeManager;

public class UnusedFromAnstop {
//
//
//    GestureOverlayView gestureOverlay;
//    GestureLibrary gestureLibrary;
//
//    AnstopDbAdapter dbHelper;
//
//
//    void onCreate(){
//
//        gestureLibrary = GestureLibraries.fromRawResource(this, R.raw.gestures);
//        gestureLibrary.load();
//
//
//        setupGesture()
//    }
//
//    void readSettings(){
//
//        if(settings.getBoolean("first_start", true)) {
//            // Show once: "Tip: You can swipe left or right, to change the mode!"
//            Toast.makeText(getApplicationContext(), R.string.first_start, Toast.LENGTH_LONG).show();
//            SharedPreferences.Editor outPref = settings.edit();
//            outPref.putBoolean("first_start", false);
//            outPref.apply();
//        }
//    }
//
//
//    /**
//     * Set up swipe gesture, based on the current mode and layout.
//     */
//    private fun setupGesture() {
//        val layout = getLayout(clock.mode)  // layout for entire activity
//        gestureOverlay = GestureOverlayView(this)
//        val mainViewGroup = findViewById(R.id.mainLayout) as ViewGroup
//
//        // remove before add, in case layout is ScrollView and can have only 1 child:
//        (mainViewGroup.parent as ViewGroup).removeView(mainViewGroup)  // avoid "specified child already has a parent"
//        layout!!.removeAllViews()  // avoid "ScrollView can host only one direct child"
//        layout.addView(gestureOverlay, LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT, 1f))
//
//        gestureOverlay.addView(mainViewGroup)
//        gestureOverlay.addOnGesturePerformedListener(this)
//        gestureOverlay.isGestureVisible = false
//    }
//
//
//    /**
//     * When the user swipes left or right, change to the previous/next mode
//     * only if the clock isn't running.
//     */
//    override fun onGesturePerformed(overlay: GestureOverlayView, gesture: Gesture) {
//        if (clock.isStarted)
//            return
//
//        val predictions = gestureLibrary.recognize(gesture)
//        for (prediction in predictions) {
//            if (prediction.score > 1.0) {
//                val cprev = clock.mode
//                if (prediction.name == "SwipeRight") {
//                    changeModeOrPopupConfirm(false, cprev, cprev.next())
//                } else if (prediction.name == "SwipeLeft") {
//                    changeModeOrPopupConfirm(true, cprev, cprev.prev())
//                }
//            }
//        }
//    }
//
//    /**
//     * Either change the mode immediately, or bring up a popup dialog to
//     * have the user confirm the mode change because clock.wasStarted or
//     * a comment was typed.
//     * Calls [popupConfirmChangeMode].
//     * <P>
//     * Assumes clock is not currently running, or mode change menu items
//     * would be disabled and swipes ignored.
//     *
//     * @param animateToLeft  True if decrementing the mode, false if incrementing
//     * @param currMode  The current mode; passed to [animateSwitch]
//     * @param newMode  The new mode if confirmed; passed to [Clock.changeMode]
//     */
//    private fun changeModeOrPopupConfirm(animateToLeft: Boolean, currMode: Mode, newMode: Mode) {
//        if (clock.wasStarted || comment != null && comment!!.length > 0) {
//            popupConfirmChangeMode(animateToLeft, currMode, newMode)
//        } else {
//            clock.changeMode(newMode)
//            if (currMode != newMode) {
//                animateSwitch(animateToLeft, currMode)
//            } else {
//                if (newMode == Mode.STOP_LAP)
//                    stopwatch()
//                else
//                    countdown()
//            }
//        }
//    }
//
//    /**
//     * Show a popup dialog to have the user confirm swiping to a different mode,
//     * which will clear the current laps and comment (if any).
//     * <P>
//     * If the mode change is confirmed: Will call [animateSwitch]
//     * only if the modes are different, otherwise will reset the layout without animation.
//     </P> * <P>
//     * Call this method only if we need to ask, because clock.wasStarted or a comment was typed.
//     * @param toRight  True if the old mode is exiting to the right, and the new mode coming in from the left;
//     * passed to [animateSwitch].
//     * Opposite direction from the swipe direction;
//     * incrementing the mode passes <tt>false</tt> here.
//     * @param currMode  The current mode; passed to [animateSwitch]
//     * @param newMode  The new mode if confirmed; passed to [Clock.changeMode]
//     */
//
//    private fun popupConfirmChangeMode(toRight: Boolean, currMode: Mode, newMode: Mode) {
//        val alert = AlertDialog.Builder(this@Anstop)
//        alert.setTitle(R.string.confirm)
//        alert.setMessage(R.string.confirm_change_mode_message)
//
//        alert.setPositiveButton(R.string.change, DialogInterface.OnClickListener { dialog, whichButton ->
//                clock.changeMode(newMode)
//            if (currMode != newMode) {
//                animateSwitch(toRight, currMode)
//            } else {
//                if (newMode == Mode.STOP_LAP)
//                    stopwatch()
//                else
//                    countdown()
//            }
//        })
//
//        alert.setNegativeButton(android.R.string.cancel) { dialog, whichButton -> }
//
//        alert.show()
//    }
//
//    /**
//     * Animate changing the current mode after a SwipeRight or SwipeLeft gesture.
//     * You must call [Clock.changeMode] before calling.  Uses [AnimationUtils].
//     * Calls [updateModeMenuFromCurrent] for the new mode.
//     * <P>
//     * Note: This method's animations do not cause [onPause] or [onResume].
//     *
//     * @param toRight  True if the old mode is exiting to the right, and the new mode coming in from the left
//     * @param modeBefore  The previous mode, before [Clock.changeMode] was called
//     * @see [onGesturePerformed]
//     */
//
//    private fun animateSwitch(toRight: Boolean, modeBefore: Mode) {
//
//        val animation = AnimationUtils.makeOutAnimation(this, toRight)
//
//        val layout = getLayout(modeBefore)
//        animation.setAnimationListener(object : AnimationListener {
//            //@Override
//            override fun onAnimationStart(animation: Animation) {}
//
//            //@Override
//            override fun onAnimationRepeat(animation: Animation) {}
//
//            //@Override
//            override fun onAnimationEnd(animation: Animation) {
//                when (clock.mode) {
//                    Anstop.Mode.COUNTDOWN -> countdown()
//                    Anstop.Mode.STOP_LAP -> stopwatch()
//                }
//                updateModeMenuFromCurrent()
//                val inAnim = AnimationUtils.makeInAnimation(this@Anstop, toRight)
//                getLayout(clock.mode)!!.startAnimation(inAnim)
//            }
//        })
//        layout!!.startAnimation(animation)
//
//    }
}
