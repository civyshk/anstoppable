/*
 * Copyright (c) 2018 by civyshk
 * civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.project104.anstoppable.counter

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.WindowManager

import kotlinx.android.synthetic.main.activity_counter_editor.*
import kotlinx.android.synthetic.main.content_counter_editor.*
import net.project104.anstoppable.*


class ActivityCounterEditor : ThemedActivity(
        R.style.AnstopThemeLight_NoActionBar,
        R.style.AnstopThemeDark_NoActionBar
) {

    private var counterId: Long = -1
    private lateinit var groupsAdapter: EditorGroupAdapter
    private lateinit var buttonsAdapter: EditorButtonAdapter

    private var changed = false

    private lateinit var title: String
    private lateinit var groupTitles: ArrayList<String>
    private lateinit var buttonValues: ArrayList<Int>
    private lateinit var groupCounts : ArrayList<Int>

    override fun onCreate(savedInstanceState: Bundle?) {
        logd("onCreate()")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_counter_editor)
        setSupportActionBar(toolbar)

        // Get the Counter to load

        var data: Bundle? = savedInstanceState
        counterId = data?.getLong("$PACKAGE.TaskId", -1L) ?: -1L

        if(counterId == -1L){
            data = intent.extras
            counterId = data!!.getLong("$PACKAGE.TaskId", -1L)
        }

        if(counterId == -1L){
            Log.e(getClassTag(this), "onCreate(); Can't get task id from bundles")
            return
        }

        data = data!!

        // Get Counter data from intent

        title = data.getString("$PACKAGE.Title")!!
        groupTitles = data.getStringArrayList("$PACKAGE.GroupTitles")!!
        groupCounts = data.getIntegerArrayList("$PACKAGE.GroupCounts")!!
        buttonValues = data.getIntegerArrayList("$PACKAGE.ButtonValues")!!
        changed = data.getBoolean("$PACKAGE.Changed", false)

        if(groupTitles.size != groupCounts.size ||
                groupTitles.isEmpty() ||
                groupCounts.isEmpty() ||
                buttonValues.isEmpty()){

            return
        }

        // Set up layout, with adapters, click listeners and all that

        /**
         * Use this class to set [changed] to true
         * when the title's EditText is modified
         *
         * @param itemIndex The index of the group/button that has been modified
         * @param saver A function which will be called whenever the EditText is
         * modified, accepting the item index and the new text as parameters
         */
        class TitleSaver : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                changed = true
            }
        }

        with(etTitle){
            setText(title)
            addTextChangedListener(TitleSaver())
        }

        groupsAdapter = EditorGroupAdapter(groupTitles.zip(groupCounts).map {
            GroupBean(it.first, it.second)
        }.toMutableList()) { changed = true }

        buttonsAdapter = EditorButtonAdapter(buttonValues) { changed = true }

        updateGroupsHeader()
        updateButtonsHeader()

        rvGroups.layoutManager = LinearLayoutManager(applicationContext)
        rvGroups.itemAnimator = DefaultItemAnimator()
        rvGroups.adapter = groupsAdapter

        rvButtons.layoutManager = GridLayoutManager(applicationContext, 4)
        rvButtons.itemAnimator = DefaultItemAnimator()
        rvButtons.adapter = buttonsAdapter

        btLessGroups.setOnClickListener {
            val size = groupsAdapter.groups.size
            if(size == 1) return@setOnClickListener
            groupsAdapter.groups.removeAt(size - 1)
            groupsAdapter.notifyDataSetChanged()
            updateGroupsHeader()
            changed = true
        }

        btMoreGorups.setOnClickListener {
            val size = groupsAdapter.groups.size
            groupsAdapter.groups.add(GroupBean())
            groupsAdapter.notifyDataSetChanged()
            updateGroupsHeader()
            changed = true
        }

        btLessButtons.setOnClickListener {
            val size = buttonsAdapter.values.size
            if(size == 1) return@setOnClickListener
            buttonsAdapter.values.removeAt(size - 1)
            buttonsAdapter.notifyDataSetChanged()
            updateButtonsHeader()
            changed = true
        }

        btMoreButtons.setOnClickListener {
            val size = buttonsAdapter.values.size
            buttonsAdapter.values.add(getDefaultButtonValue(size))
            buttonsAdapter.notifyDataSetChanged()
            updateButtonsHeader()
            changed = true
        }

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
    }

    /**
     * Update this counter customizable fields with data from two sources:
     * 1. The EditText for the counter title, always available
     * 2. The data held by adapters, as the EditTexts are recycled
     *    and I can't rely on their data. The adapters should
     *    take care of maintaining their data updated whenever
     *    an EditText's text changes
     */
    private fun updateEditorFields() {
        title = etTitle.text.toString()

        groupTitles = groupsAdapter.groups.fold(ArrayList<String>()) {
            groupTitles, group ->
                groupTitles.apply { add(group.title) }
        }

        groupCounts = groupsAdapter.groups.fold(ArrayList<Int>()) {
            counts, group ->
                counts.apply { add(group.count) }
        }

        buttonValues = buttonsAdapter.values.fold(ArrayList<Int>()) {
            values, value ->
                values.apply { add(value) }
        }
    }

    private fun fillBundle(outState: Bundle) {
        outState.putLong("$PACKAGE.TaskId", counterId)
        outState.putString("$PACKAGE.Title", title)
        outState.putStringArrayList("$PACKAGE.GroupTitles", groupTitles)
        outState.putIntegerArrayList("$PACKAGE.GroupCounts", groupCounts)
        outState.putIntegerArrayList("$PACKAGE.ButtonValues", buttonValues)
        outState.putBoolean("$PACKAGE.Changed", changed)
    }

    private fun fillIntent(result: Intent) {
        result.putExtra("$PACKAGE.TaskId", counterId)
        result.putExtra("$PACKAGE.Title", title)
        result.putStringArrayListExtra("$PACKAGE.GroupTitles", groupTitles)
        result.putIntegerArrayListExtra("$PACKAGE.GroupCounts", groupCounts)
        result.putIntegerArrayListExtra("$PACKAGE.ButtonValues", buttonValues)
        result.putExtra("$PACKAGE.Changed", changed)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        if(outState == null)
            return

        updateEditorFields()
        fillBundle(outState)
    }

    private fun updateGroupsHeader(){
        tvNumGroups.text = resources.getQuantityString(R.plurals.available_groups,
                groupsAdapter.groups.size, groupsAdapter.groups.size)
    }

    private fun updateButtonsHeader(){
        tvNumButtons.text = resources.getQuantityString(R.plurals.available_buttons,
                buttonsAdapter.values.size, buttonsAdapter.values.size)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_activity_editor_counter, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_discard -> {
                if(changed){
                    showDialogDiscard()
                }else{
                    setResult(AppCompatActivity.RESULT_CANCELED)
                    finish()
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        if(changed){
            showDialogSave()
        }else {
            super.onBackPressed()
        }
    }

    private fun showDialogDiscard() {
        val builder = AlertDialog.Builder(this@ActivityCounterEditor)
        builder.setTitle(R.string.dialog_counter_discard_title)
                .setMessage(R.string.dialog_counter_discard_message)
                .setPositiveButton(R.string.dialog_counter_dont_save){dialog, which ->
                    setResult(AppCompatActivity.RESULT_CANCELED)
                    finish()
                }
                .setNegativeButton(R.string.dialog_counter_cancel){dialog,which ->
                }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun showDialogSave() {
        val builder = AlertDialog.Builder(this@ActivityCounterEditor)
        builder.setTitle(R.string.dialog_counter_save_title)
                .setMessage(R.string.dialog_counter_save_message)
                .setPositiveButton(R.string.dialog_counter_save){dialog, which ->
                    setResult(AppCompatActivity.RESULT_OK, getIntentWithResult())
                    finish()
                }
                .setNegativeButton(R.string.dialog_counter_dont_save){dialog,which ->
                    setResult(AppCompatActivity.RESULT_CANCELED)
                    finish()
                }
                .setNeutralButton(R.string.dialog_counter_cancel){_,_ ->
                }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun getIntentWithResult() : Intent {
        updateEditorFields()
        return Intent().also( ::fillIntent )
    }

}

/**
 * This class is used by [EditorButtonAdapter] & [EditorGroupAdapter]
 * to update their respective counter data when some EditText changes its content
 *
 * @param itemIndex The index of the group/button that has been modified
 * @param saver A function which will be called whenever the EditText is
 * modified, accepting the item index and the new text as parameters
 */
class ItemTextSaver(private val itemIndex: Int, private val saver: (index: Int, text: String) -> Unit) : TextWatcher {
    override fun afterTextChanged(s: Editable?) {}
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        saver(itemIndex, s.toString())
    }
}