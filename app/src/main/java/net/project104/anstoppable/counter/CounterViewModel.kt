/*
 * Copyright (c) 2018 by civyshk
 * civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.project104.anstoppable.counter

import android.app.Application
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.util.Log
import net.project104.anstoppable.MyApp
import net.project104.anstoppable.TaskViewModel
import net.project104.anstoppable.db.TaskDao
import net.project104.anstoppable.getClassTag
import net.project104.anstoppable.handleException
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import kotlin.math.min

typealias DBCounter = net.project104.anstoppable.db.Counter
typealias DBGroup = net.project104.anstoppable.db.Group
typealias DBButton = net.project104.anstoppable.db.Button

class GroupBean(var title: String = "", var count: Int = 0)

class CounterViewModel(app: Application)
    : TaskViewModel(app) {

    var dao: TaskDao = (app as MyApp).taskDao

    /** This is initialized in either setId() or initNew(). Call one */
    lateinit var counter: DBCounter
    var groups = mutableListOf<DBGroup>()
    var buttons = mutableListOf<DBButton>()

    /** The UI must change these fields ot trigger database updates
     * (or its own added observers)
     */
    var liveTitle = MutableLiveData<String>()
    var liveCounts = mutableListOf<MutableLiveData<Int>>()

    /** Field used externally to trigger a call to its observers.
     * The boolean value is not really used */
    private val _flagGridChanged = MutableLiveData<Boolean>()
    init{ _flagGridChanged.value = false }

    /** Attach an observer to this liveData when the entire layout
     * must be regenerated */
    fun setGridChangedObserver(
            owner: LifecycleOwner, observer: Observer<Boolean?>){

        _flagGridChanged.observe(owner, observer)
    }

    /** Use this method to trigger the attached observer */
    fun flagGridChanged(){
        _flagGridChanged.value = ! _flagGridChanged.value!!
    }

    /** Update groups and save to DB */
    fun setGroupsTitleAndCount(titles: List<String>, counts: List<Int>) {
        if (titles.size != counts.size) {
            Log.e(getClassTag(this),
                    "setGroupsTitleAndCount: titles.size=${titles.size} counts.size=${counts.size}")
            return
        }

        fun removeGroup(groupIndex: Int) {
            groups.removeAt(groupIndex).also {
                doAsync({ handleException(getClassTag(this) + ".setGroupsTitleAndCount.removeGroup", it) })
                    { dao.deleteGroup(it) }
            }
        }

        fun addGroup(groupIndex: Int, title: String, count: Int) {
            val safeTitle = title
            groups.add(DBGroup(taskId, groupIndex, safeTitle, count).also{
                doAsync({ handleException(getClassTag(this) + ".setGroupsTitleAndCount.addGroup", it) })
                { dao.insertGroup(it) }
            })


        }

        fun updateGroup(groupIndex: Int, title: String, count: Int){
            val safeTitle = title
            with(groups[groupIndex]) {
                this.title = title
                groupCount = count
                doAsync({ handleException(getClassTag(this) + ".setGroupsTitleAndCount.updateGroup", it) })
                { dao.updateGroup(this@with) }
            }
        }

        val nGroups = groups.size
        if (titles.size < nGroups) {
            (0 until titles.size).forEach { updateGroup(it, titles[it], counts[it]) }
            (titles.size until nGroups).forEach { removeGroup(titles.size) }
        } else {
            (0 until nGroups).forEach { updateGroup(it, titles[it], counts[it]) }
            (nGroups until titles.size).forEach { addGroup(it, titles[it], counts[it]) }
        }

        initCountsLivedata()
    }

    /** Update buttons and save to DB */
    fun setButtonValues(values: ArrayList<Int>) {

        fun removeButton(index: Int) {
            buttons.removeAt(index).also {
                doAsync({ handleException(getClassTag(this) + ".setButtonValues.removeButton", it) })
                { dao.deleteButton(it) }
            }
        }

        fun addButton(index: Int, value: Int) {
            buttons.add(DBButton(taskId, index, value).also{
                doAsync({ handleException(getClassTag(this) + ".setButtonValues.addButton", it) })
                { dao.insertButton(it) }
            })
        }

        fun updateButton(index: Int, value: Int) {
            with(buttons[index]){
                this.value = value
                doAsync({ handleException(getClassTag(this) + ".setButtonValues.updateButton", it) })
                { dao.updateButton(this@with) }
            }
        }

        val nButtons = buttons.size
        if (values.size < nButtons) {
            (0 until values.size).forEach{ updateButton(it, values[it]) }
            (values.size until nButtons).forEach{ removeButton(values.size) }
        }else {
            (0 until nButtons).forEach { updateButton(it, values[it]) }
            (nButtons until values.size).forEach { addButton(it, values[it]) }
        }
    }

    private fun initTitleAndCounts(){
        liveTitle.value = counter.title
        liveTitle.observeForever{
            counter.title = it!!
            doAsync({ handleException(getClassTag(this) + ".initTitleAndCounts", it) })
            { dao.updateCounter(counter) }
        }
        initCountsLivedata()
    }

    private fun initCountsLivedata() {
        this.liveCounts.clear()
        this.liveCounts.addAll(groups.mapIndexed { index, it ->
            MutableLiveData<Int>().apply {
                value = it.groupCount
                observeForever {
                    groups[index].groupCount = it!!
                    doAsync({ handleException(getClassTag(this) + ".initCountsLiveData", it) })
                    { dao.updateGroup(groups[index]) }
                }
            }
        })
    }

    /** Make this object load data from database. If the given taskId matches
     * the one saved by this object, it skips the unneeded initialization
     * but will still run the given block of code
     * @param newId: The id of the Counter task to load
     * @param uiTask: Task to run in uiThread when the data has been loaded
     */
    fun initId(newId: Long, uiTask : () -> Unit){
        Log.d(getClassTag(this@CounterViewModel), "Creating Counter from Id=$newId")
        if(taskId != newId) {
            taskId = newId

            doAsync({ handleException(getClassTag(this) + ".initId", it) })
            {
                counter = dao.getCounterByKey(taskId)
                groups = dao.getAllGroups(taskId).toMutableList()
                buttons = dao.getAllButtons(taskId).toMutableList()

                uiThread {
                    initTitleAndCounts()
                    uiTask()
                    initialized.value = true
                }
            }
        }else{
            uiTask()
        }
    }

    /** If you don't set {@link #taskId}, you must call initNew() to create a new
     * Counter and save it in the DB
     * @param uiTask: Task to run in uiThread when the data has been created and saved
     */
    fun initNew(uiTask: () -> Unit) {
        val DEFAULT_GROUPS = 2
        val DEFAULT_BUTTONS = 2

        doAsync({ handleException(getClassTag(this) + ".initNew", it) }) {
            taskId = dao.insertTask(net.project104.anstoppable.db.Task())
            Log.d(getClassTag(this@CounterViewModel), "Creating new Counter, id:$taskId")
            counter = DBCounter(id = taskId)
            dao.insertCounter(counter).toInt()
            buttons = MutableList<DBButton>(DEFAULT_BUTTONS) {
                DBButton(taskId, it, getDefaultButtonValue(it))
            }
            groups = MutableList<DBGroup>(DEFAULT_GROUPS) {
                DBGroup(taskId, it)
            }

            dao.insertCounter(counter)
            groups.forEach { dao.insertGroup(it) }
            buttons.forEach { dao.insertButton(it) }

            uiThread {
                initTitleAndCounts()
                uiTask()
                initialized.value = true
            }
        }
    }
}

/**
 * Each button can be assigned a default value according
 * to its index
 * @param index  The index of the button
 * @return 1, 5, 10, 20, 50, etc, according to index
 */
fun getDefaultButtonValue(index: Int) : Int {

    //return = 1, 5, 10, 20, 50, 100, 200, 500, etc
    //return = 1, 5, a*b
    //a =             1,  2,  5,   1,   2,   5, etc
    //b =            10, 10, 10, 100, 100, 100, etc

    if(index <= 0) return 1
    else if(index == 1) return 5

    val a = when(index%3){
        0 -> 2
        1 -> 5
        2 -> 1
        else -> 0
    }

    val exp = min((index+1)/3, 3)
    var b = 1
    for(i in 0 until exp) b*= 10

    return a*b
}

