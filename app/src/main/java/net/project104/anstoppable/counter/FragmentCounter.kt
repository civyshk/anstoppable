/*
 * Copyright (c) 2018 by civyshk
 * civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.project104.anstoppable.counter

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import net.project104.anstoppable.*
import net.project104.anstoppable.SettingsActivity


class FragmentCounter() : FragmentTask() {

    val TAG = getClassTag(this)

    lateinit var counterVM: CounterViewModel
    var nGroups: Int = 1
    var nButtons: Int = 1
    val groupParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.MATCH_PARENT,
            1.0f
    )

    /** Associate each button to its group [0,n] */
    var buttonGroup: MutableMap<Button, Int> = mutableMapOf()

    /** Associate each button to its index [0,3] */
    var buttonIndex: MutableMap<Button, Int> = mutableMapOf()

    // GUI
    lateinit var rootView: ViewGroup
    lateinit var tvTitle: TextView
    lateinit var layGroups: ViewGroup
    val groupLayouts = mutableListOf<ViewGroup>()
    val groupTvTitles: MutableList<TextView> = mutableListOf()
    val groupTvCounts: MutableList<TextView> = mutableListOf()
    val buttons: MutableList<MutableList<Button>> = mutableListOf()

    /**
     * Fragment constructor arguments are better passed within
     * a Bundle. Use this to mimic that
     */
    @SuppressLint("ValidFragment")
    constructor (taskId: Long) : this(){
        arguments = Bundle().apply {
            putLong("TaskId", taskId)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        counterVM = ViewModelProviders.of(this).get(CounterViewModel::class.java)

        //Try to get desired taskId from activity arguments or savedInstanceState
        taskId = arguments?.getLong("TaskId", -1L) ?:
                (savedInstanceState?.getLong("TaskId", -1L) ?:
                -1L)

        //If none, maybe clockVM saved its state
        if (taskId == -1L) {
            taskId = counterVM.taskId
        }

        // Decide whether to init a new Counter or load a previous one
        if(taskId == -1L){
            counterVM.initNew {
                taskId = counterVM.taskId
                saveCurrentTaskInPreferences()
            }
        }else{
            counterVM.initId(taskId) {
                taskId = counterVM.taskId
                saveCurrentTaskInPreferences()
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        logd("onCreateView()")

        //Inflate empty layout
        rootView = inflater.inflate(R.layout.fragment_counter, container, false) as ViewGroup
        layGroups = rootView.findViewById<ViewGroup>(R.id.layGroups)!!
        tvTitle = rootView.findViewById<TextView>(R.id.tvTitle)!!

        //The code inside the observer is meant to run AFTER the layout fields exist
        // and after the counterVM is fully initialized, so here at onCreateView
        //is a good place to add it
        //This approach allows the viewModel to load its data as early as possible
        counterVM.initialized.observe(this@FragmentCounter,
                Observer<Boolean> { initialized ->
                    if(initialized!!) {
                        initLayoutAfterViewModel(inflater)
                        counterVM.initialized.removeObservers(this@FragmentCounter)
                    }
        })

        layGroups.setOnLongClickListener{ _ ->
            startEditor()
            true
        }

        tvTitle.setOnLongClickListener {
            startEditor()
            true
        }

        return rootView
    }

    private fun initLayoutAfterViewModel(inflater: LayoutInflater) {
        loadCounterLayout(inflater)

        setObserverForTitle()
        setObserversForCounts()
        setObserverForGridReset()
    }

    private fun loadCounterLayout(inflater: LayoutInflater = activity!!.layoutInflater) {
        tvTitle.text = counterVM.liveTitle.value!!

        addGroupsAndButtons(inflater)
        rootView.findViewById<Button>(R.id.btReset).setOnClickListener(::resetCounters)
    }

    private fun clearGroupsAndButtons(){
        layGroups.removeAllViews()
        listOf(buttons, groupTvCounts, groupTvTitles, groupLayouts).forEach{ it.clear() }
        listOf(buttonGroup, buttonIndex).forEach{ it.clear() }
    }

    private fun resetAllGroupsAndButtons() {
        clearGroupsAndButtons()
        if (activity != null){
            addGroupsAndButtons(activity!!.layoutInflater)
            setObserversForCounts()
        }
    }

    private fun addGroupsAndButtons(inflater: LayoutInflater) {
        nGroups = getNumberGroups()
        nButtons = getNumberButtons()
        for (i in 0 until nGroups) {
            addGroup(i, inflater)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
//        hackMenuIcons(menu)
//        inflater.inflate(R.menu.menu_main, menu)
        inflater.inflate(R.menu.menu_fragment_counter, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_settings -> {
                val i = Intent()
                i.setClass(activity, SettingsActivity::class.java)
                i.putExtra("TaskType", TaskType.COUNTER.asInt)
                startActivityForResult(i, 0) // 0 unused
                return true
            }
            R.id.action_edit -> {
                startEditor()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun startEditor() {
        val intent = Intent(activity, ActivityCounterEditor::class.java)

        intent.putExtra("$PACKAGE.Title", counterVM.liveTitle.value!!)
        intent.putStringArrayListExtra("$PACKAGE.GroupTitles",
                counterVM.groups.fold(ArrayList<String>()){
                    groupTitles, group ->
                    groupTitles.add(group.title)
                    groupTitles
                })
        intent.putIntegerArrayListExtra("$PACKAGE.GroupCounts",
                counterVM.groups.fold(ArrayList<Int>()){
                    counts, group ->
                    counts.add(group.groupCount)
                    counts
                })
        intent.putIntegerArrayListExtra("$PACKAGE.ButtonValues",
                counterVM.buttons.fold(ArrayList<Int>()){
                    values, button ->
                    values.add(button.value)
                    values
                })

        intent.putExtra("$PACKAGE.TaskId", taskId)
        startActivityForResult(intent, 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(data == null) return
//        if(requestCode != 1) return
        if(resultCode != AppCompatActivity.RESULT_OK) return

        val taskId = data.getLongExtra("$PACKAGE.TaskId", -1L)
        if(taskId != this.taskId){
            Log.e(TAG, "onActivityResult: taskIds don't match")
        }
        val title = data.getStringExtra("$PACKAGE.Title")!!
        val groupTitles = data.getStringArrayListExtra("$PACKAGE.GroupTitles")!!
        val groupCounts = data.getIntegerArrayListExtra("$PACKAGE.GroupCounts")!!
        val buttonValues = data.getIntegerArrayListExtra("$PACKAGE.ButtonValues")!!

        counterVM.liveTitle.value = title
        counterVM.setGroupsTitleAndCount(groupTitles, groupCounts)
        counterVM.setButtonValues(buttonValues)
        counterVM.flagGridChanged()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putLong("TaskId", taskId)
    }

    fun getNumberGroups() = counterVM.groups.size
    fun getNumberButtons() = counterVM.buttons.size

    private fun addGroup(group: Int, inflater: LayoutInflater){
        val groupLayout = inflater.inflate(R.layout.counter_group, layGroups, false) as ViewGroup
        layGroups.addView(groupLayout, groupParams)
        groupLayouts.add(groupLayout)

        val tvGroupTitle = groupLayout.findViewWithTag<TextView>("tvGroupTitle")!!
        tvGroupTitle.text = counterVM.groups[group].title

        groupTvTitles.add(tvGroupTitle)
        groupTvCounts.add(groupLayout.findViewWithTag("tvCount")!!)
        buttons.add(mutableListOf())
        counterVM.buttons.forEachIndexed { index, it->
            addButton(group, index, it.value, inflater)
        }
    }

    private fun addButton(group: Int, index: Int, value: Int, inflater: LayoutInflater){
        val groupView = groupLayouts[group]
        val btn = inflater.inflate(R.layout.counter_button, groupView, false) as Button
        groupView.addView(btn)
        btn.setOnClickListener(::buttonClicked)
//        btn.text = if (value <= 0) value.toString() else "+$value"
        btn.text = value.toString()
        buttonGroup[btn] = group
        buttonIndex[btn] = index
        buttons[group].add(btn)
    }

    private fun buttonClicked(view: View){
        val group = buttonGroup[view]!!
        val buttonIndex = buttonIndex[view]!!
        val valueNow = counterVM.liveCounts[group].value!!
        val valueAfter = valueNow + counterVM.buttons[buttonIndex].value

        counterVM.liveCounts[group].value = valueAfter
    }

    private fun resetCounters(view: View) {
        counterVM.liveCounts.forEach { it.value = 0 }
    }

    private fun updateGuiTitle(title: String) {
        tvTitle.text = title
    }

    private fun updateGuiCount(group: Int, count: Int?) {
        groupTvCounts[group].text = count!!.toString()
    }


    private fun setObserverForGridReset() {
        counterVM.setGridChangedObserver(this, Observer<Boolean?> { _ ->
            resetAllGroupsAndButtons()
            //This <del>might be</del> is stupid as I'll only call resetAllGroupsAndButtons
            //from this fragment, so I don't really need that flag and observer.
            //Duh, it works
        })
    }

    private fun setObserverForTitle() {
        counterVM.liveTitle.observe(this, Observer<String> { title ->
            updateGuiTitle(if(title.isNullOrBlank())
                getString(R.string.counter)
            else
                title!!)
        })
    }

    private fun setObserversForCounts() {
        //Note: Partially updating titles, counts or buttons is unsupported right now
        counterVM.liveCounts.forEachIndexed { index, it ->
            it.observe(this, Observer<Int> { count ->
                updateGuiCount(index, count)
            })
        }
    }
}
