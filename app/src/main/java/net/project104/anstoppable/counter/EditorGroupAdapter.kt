/*
 * Copyright (c) 2018 by civyshk
 * civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.project104.anstoppable.counter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.text.TextWatcher
import net.project104.anstoppable.R


/**
 * @param groups List with all the info needed for groups, as [GroupBean]
 * @param changeListener This function will be called every time that
 * the count or the title of a group is modified
 */
class EditorGroupAdapter(val groups: MutableList<GroupBean>,
                         val changeListener: () -> Unit)
    : RecyclerView.Adapter<GroupViewHolder>() {

    private val titleWatchers = mutableMapOf<EditText, TextWatcher>()
    private val countWatchers = mutableMapOf<EditText, TextWatcher>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GroupViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.editor_counter_group_item, parent, false)

        return GroupViewHolder(itemView)
    }

    override fun getItemCount(): Int = groups.size

    override fun onBindViewHolder(holder: GroupViewHolder, position: Int) {
        val group = groups[position]

        with(holder.etCount){
            removeTextChangedListener(countWatchers[this])
            setText(group.count.toString())
            countWatchers[this] = ItemTextSaver(position, ::saveCount)
            addTextChangedListener(countWatchers[this])
        }

        with(holder.etTitle){
            removeTextChangedListener(titleWatchers[this])
            setText(group.title)
            titleWatchers[this] = ItemTextSaver(position, ::saveTitle)
            addTextChangedListener(titleWatchers[this])
        }
    }

    /**
     * Save a new group count and call [changeListener]
     * @param groupIndex The index of the modified group
     * @param countStr The text to be converted into a new integer group count
     */
    private fun saveCount(groupIndex: Int, countStr: CharSequence) {
        groups[groupIndex].count = try {
            countStr.toString().toInt()
        } catch (e: NumberFormatException) {
            0
        }
        changeListener()
    }

    /**
     * Save a new group title and call [changeListener]
     * @param groupIndex The index of the modified group
     * @param title The new title
     */
    private fun saveTitle(groupIndex: Int, title: CharSequence) {
        groups[groupIndex].title = title.toString()
        changeListener()
    }
}

class GroupViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    var etCount: EditText = view.findViewWithTag<EditText>("etCount")
    var etTitle: EditText = view.findViewWithTag<EditText>("etTitle")
}