/*
 * Copyright (c) 2018 by civyshk
 * civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.project104.anstoppable.counter

import android.support.v7.widget.RecyclerView
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import net.project104.anstoppable.R

/**
 * @param values List with the button integer values
 * @param changeListener This function will be called every time that
 * any button value is modified
 */
class EditorButtonAdapter(val values: MutableList<Int>,
                          val changeListener: () -> Unit)
    : RecyclerView.Adapter<ButtonViewHolder>() {

    /** I must store the TextWatcher listeners I use because EditText
     * doesn't have a method to clear TextWatcher listeners. */
    private val buttonWatchers = mutableMapOf<EditText, TextWatcher>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            : ButtonViewHolder
            = ButtonViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.editor_counter_button_item, parent, false))

    override fun getItemCount(): Int = values.size

    override fun onBindViewHolder(holder: ButtonViewHolder, position: Int) {
        with(holder.etValue){
            removeTextChangedListener(buttonWatchers[this])
            setText(values[position].toString())
            buttonWatchers[this] = ItemTextSaver(position, ::saveValue)
            addTextChangedListener(buttonWatchers[this])
        }
    }

    /**
     * Save a new button value and call [changeListener]
     * @param index The index of the modified button
     * @param valueStr The text to be converted into a new integer button value
     */
    private fun saveValue(index: Int, valueStr: CharSequence) {
        values[index] = try {
            valueStr.toString().toInt()
        } catch (e: NumberFormatException) {
            1
        }
        changeListener()
    }
}

class ButtonViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    var etValue: EditText = view.findViewWithTag<EditText>("etValue")
}