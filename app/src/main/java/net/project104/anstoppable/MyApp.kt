/*
 * Copyright (c) 2018 by civyshk
 * civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.project104.anstoppable

import android.app.Application
import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Room
import android.arch.persistence.room.migration.Migration
import android.util.Log
import android.view.Menu
import net.project104.anstoppable.clock.AnstopDbAdapter
import net.project104.anstoppable.clock.HourFormat
import net.project104.anstoppable.clock.LapFormatter.Companion.LAP_FMT_FLAG_ELAPSED
import net.project104.anstoppable.clock.ThemeManager
import net.project104.anstoppable.db.TaskDao
import net.project104.anstoppable.db.DatabaseHelper
import net.project104.anstoppable.db.MyDatabase

const val PACKAGE = "net.project104.filetecontador"
const val PREF_FILE = "$PACKAGE.preferences"
const val PREF_LAST_TASK = "LastTask"

lateinit var PREF_DARK_THEME: String
lateinit var PREF_LAP_FORMAT_PREFIX: String
lateinit var PREF_LAP_FORMAT_ELAPSED: String
lateinit var PREF_LAP_FORMAT_DELTA: String
lateinit var PREF_LAP_FORMAT_SYSTIME: String
lateinit var PREF_NUMBER_DECIMALS: String
lateinit var PREF_HOUR_FORMAT: String
lateinit var PREF_VIBRATE: String
lateinit var PREF_VIBRATE_COUNTDOWN_0: String
lateinit var PREF_USE_MOTION_SENSOR: String

class MyApp : Application(){

    lateinit var database: MyDatabase
    lateinit var dbHelper: DatabaseHelper
    lateinit var taskDao: TaskDao
    lateinit var anstopDbHelper: AnstopDbAdapter
    lateinit var themeManager: ThemeManager

    override fun onCreate() {
        super.onCreate()

        PREF_DARK_THEME = getString(R.string.pref_dark_theme)
        PREF_USE_MOTION_SENSOR = getString(R.string.pref_use_motion_sensor)
        PREF_VIBRATE = getString(R.string.pref_vibrate)
        PREF_VIBRATE_COUNTDOWN_0 = getString(R.string.pref_vibrate_countdown_0)
        PREF_HOUR_FORMAT = getString(R.string.pref_hour_format)
        PREF_LAP_FORMAT_PREFIX = getString(R.string.pref_lap_format_prefix)
        PREF_LAP_FORMAT_ELAPSED = getString(R.string.pref_lap_format_elapsed)
        PREF_LAP_FORMAT_DELTA = getString(R.string.pref_lap_format_delta)
        PREF_LAP_FORMAT_SYSTIME = getString(R.string.pref_lap_format_systime)
        PREF_NUMBER_DECIMALS = getString(R.string.pref_number_decimals)

        database = Room.databaseBuilder(applicationContext, MyDatabase::class.java, "database")
                .addMigrations(FROM_2_TO_3)
                .build()

        taskDao = database.taskDao()
        dbHelper = DatabaseHelper(taskDao)
        anstopDbHelper = AnstopDbAdapter(this)
        anstopDbHelper.open()
        anstopDbHelper.deleteTemporaryLaps()
        themeManager = ThemeManager(this)

    }

    companion object {
        val FROM_2_TO_3 = Migration2To3()
        val APP_VERSION = "1.0"
    }
    //anstopDbHelper.close()  // prevent leaks //WHERE?

}

class Migration2To3 : Migration(2, 3) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("ALTER TABLE Clock ADD COLUMN hourFormat INT NOT NULL DEFAULT ${HourFormat.HIDE_IF_0.asInt}")
        database.execSQL("ALTER TABLE Clock ADD COLUMN numberDecimals INT NOT NULL DEFAULT 1")
        database.execSQL("ALTER TABLE Clock ADD COLUMN lapFormatFlags INT NOT NULL DEFAULT $LAP_FMT_FLAG_ELAPSED")
    }
}

fun getClassTag(who: Any) : String {
    return "${who.hashCode()%1000} Fil-Con:${who.javaClass.simpleName}"
}

/**
 * Reflection hack to show icons in menu
 * It's an extension function so it can use [logw]
 * @param menu
 */
fun Any.hackMenuIcons(menu: Menu) {
    if (menu.javaClass.simpleName == "MenuBuilder") {
        try {
            val m = menu.javaClass.getDeclaredMethod(
                    "setOptionalIconsVisible", java.lang.Boolean.TYPE)
            m.isAccessible = true
            m.invoke(menu, true)
        } catch (e: NoSuchMethodException) {
            logw("Reflection failed. Can't show icons in menu", e)
        } catch (e: IllegalAccessException) {
            logw("Reflection failed. Can't show icons in menu", e)
        } catch (e: IllegalArgumentException) {
            logw("Reflection failed. Can't show icons in menu", e)
        } catch (e: java.lang.reflect.InvocationTargetException) {
            logw("Reflection failed. Can't show icons in menu", e)
        }

    }
}

//TODO use debugLog across all the app
///**
// * Add this to the Debug Log, if [DEBUG_LOG_ENABLED].
// */
//fun Any.addDebugLog(msg: CharSequence) {
//    if (!DEBUG_LOG_ENABLED)
//        return
//
//    if (fmt_debuglog_time == null)
//        fmt_debuglog_time = buildDateFormat(activity!!, true)
//
//    debugLog.append(DateFormat.format(fmt_debuglog_time, System.currentTimeMillis()))
//    debugLog.append(": ")
//    debugLog.append(msg)
//    debugLog.append("\n")
//}


fun Any.logd(msg: String) { Log.d(getClassTag(this), msg) }
fun Any.logw(msg: String) { Log.w(getClassTag(this), msg) }
fun Any.loge(msg: String) { Log.e(getClassTag(this), msg) }

fun Any.logd(msg: String, tr: Throwable) { Log.d(getClassTag(this), msg, tr) }
fun Any.logw(msg: String, tr: Throwable) { Log.w(getClassTag(this), msg, tr) }
fun Any.loge(msg: String, tr: Throwable) { Log.e(getClassTag(this), msg, tr) }
