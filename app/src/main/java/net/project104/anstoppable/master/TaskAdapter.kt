/*
 * Copyright (c) 2018 by civyshk
 * civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.project104.anstoppable.master

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.view.View
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import net.project104.anstoppable.R
import net.project104.anstoppable.TaskType
import net.project104.anstoppable.db.ClockMaster
import net.project104.anstoppable.db.TaskMaster


class TaskAdapter(val context: Context,
                  val tasks: MutableList<TaskMaster>,
                  val clickAction: (taskId: Long, type: TaskType) -> Unit,
                  val deleteAction: (taskId: Long) -> Unit) : RecyclerView.Adapter<TaskViewHolder>() {

    private val icons: List<Drawable>

    private val clocksThread = MasterClocksThread().apply { start() }

    init {
        // Load themed icons ids

        // Create an array of the attributes we want to resolve
        val attrs = intArrayOf(R.attr.icon_counter, R.attr.icon_stopwatch, R.attr.icon_countdown)

        // Obtain the styled attributes.
        val styledAttributes = context.obtainStyledAttributes(attrs)

        icons = (0..2).map {
            styledAttributes.getDrawable(it)
        }

        // Free the resources used by TypedArray
        styledAttributes.recycle()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            : TaskViewHolder = TaskViewHolder(
                LayoutInflater.from(parent.context)
                        .inflate(R.layout.master_task_item, parent, false),
                clickAction, deleteAction)

    override fun getItemCount(): Int = tasks.size

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        with(holder) {
            val task = tasks[position]
            taskId = task.taskId
            type = task.type

            // Set appropriate icon
            ivTaskType.setImageDrawable(when (task.type) {
                TaskType.COUNTER -> icons[0]
                TaskType.STOPWATCH -> icons[1]
                TaskType.COUNTDOWN -> icons[2]
            })

            // Set title of task
            tvTaskTitle.text = if (task.title.isEmpty()) {
                context.resources.getString(when (type) {
                    TaskType.STOPWATCH -> R.string.stopwatch
                    TaskType.COUNTDOWN -> R.string.countdown
                    TaskType.COUNTER -> R.string.counter
                })
            } else {
                task.title
            }

            // Set detail text for task. On clock tasks, it's controlled and
            // updated by [MasterClocksThread]
            if (task.type == TaskType.STOPWATCH || task.type == TaskType.COUNTDOWN) {

                // First of all, stop any miniclock updating this detail textview
                // This must be done before any other miniclock is given this textview
                clocksThread.removeClock(tvTaskDetail)

                // Try to reuse an existing miniclock or create a new one
                var miniClock = clocksThread.getClock(tvTaskDetail)
                if (miniClock != null) {
                    miniClock.reset((task as ClockMaster).dbClock, tvTaskDetail)
                }else {
                    miniClock = MiniClock((task as ClockMaster).dbClock, tvTaskDetail, context.resources)
                }

                clocksThread.setClock(tvTaskDetail, miniClock)
            } else {
                clocksThread.removeClock(tvTaskDetail)
                tvTaskDetail.text = task.getDetail()
            }
        }
    }

    /** Remove a task from its backing collection, if it's there */
    fun removeTask(taskId: Long) {
        for (position in 0 until tasks.size) {
            if (tasks[position].taskId == taskId) {
                tasks.removeAt(position)
                break
            }
        }
    }

}

class TaskViewHolder(view: View,
                     val clickAction: (taskId: Long, type: TaskType) -> Unit,
                     val deleteAction: (taskId: Long) -> Unit)
    : RecyclerView.ViewHolder(view) {

    var ivTaskType = view.findViewWithTag<ImageView>("ivTaskType")
    var tvTaskTitle = view.findViewWithTag<TextView>("tvTaskTitle")
    var tvTaskDetail = view.findViewWithTag<TextView>("tvTaskDetail")
    var btDelete = view.findViewWithTag<ImageButton>("btDeleteTask")

    var taskId: Long = -1L
    lateinit var type: TaskType

    init {
        view.setOnClickListener { clickAction(taskId, type) }
        btDelete.setOnClickListener { deleteAction(taskId) }
    }

}