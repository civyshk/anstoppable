/*
 * Copyright (c) 2018 by civyshk
 * civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.project104.anstoppable.master

import android.widget.TextView
import net.project104.anstoppable.clock.SplitTime
import net.project104.anstoppable.logd
import org.jetbrains.anko.doAsyncResult
import java.util.concurrent.locks.ReadWriteLock
import java.util.concurrent.locks.ReentrantReadWriteLock
import kotlin.math.max

class MasterClocksThread : Thread() {

    /** Each TextView is associated to a [MiniClock] which
     * will format and create its text */
    private val miniClocks: MutableMap<TextView, MiniClock> = mutableMapOf()
    private val miniClocksLock = ReentrantReadWriteLock()

    private var ticks: Long = 0L
    private var startTime: Long = 0L
    private var threadSleepTime: Long = 100L
    private val currentTime: Long
        get() = System.currentTimeMillis() - startTime

    override fun run() {
        logd("run(); Starting thread")
        startTime = System.currentTimeMillis()

        while(!Thread.interrupted()){
            tickAllClocks()

            ++ticks
            if(ticks % 10 == 0L)
                adjustSleepTime()

            try {
                Thread.sleep(threadSleepTime)
            } catch (e: InterruptedException) {
                return
            }
        }

        logd("run(); Exiting thread")
    }

    /** Remove a [MiniClock] and keep it from
     * updating the view */
    internal fun removeClock(view: TextView) {
        miniClocksLock.writeLock().lock()
        try{
            miniClocks.remove(view)
        }finally {
            miniClocksLock.writeLock().unlock()
        }
    }

    /** Get a [MiniClock] thread safely */
    internal fun getClock(view: TextView) : MiniClock? {
        miniClocksLock.readLock().lock()
        try {
            return miniClocks[view]
        } finally {
            miniClocksLock.readLock().unlock()
        }
    }

    /** Associate a [MiniClock] to a TextView */
    internal fun setClock(view: TextView, clock: MiniClock) {
        miniClocksLock.writeLock().lock()
        try {
            miniClocks[view] = clock
        } finally {
            miniClocksLock.writeLock().unlock()
        }
    }

    /** Loop through every clock and tick it one decisecond */
    private fun tickAllClocks() {
        miniClocksLock.readLock().lock()
        try {
            miniClocks.values.forEach(MiniClock::tickDecisecond)
        } finally {
            miniClocksLock.readLock().unlock()
        }
    }

    /**
     * Because Thread.sleep(100) can sleep not exactly 100 ms,
     * call this method regularly to gracefully adjust the
     * sleep time.
     * If not, GUI clocks will probably diverge from actual clock
     */
    private fun adjustSleepTime(){
        val approximateTime = SplitTime(ticks).asMilliseconds()
        threadSleepTime = max(0, 100 + (approximateTime - currentTime) / 10)
    }
}