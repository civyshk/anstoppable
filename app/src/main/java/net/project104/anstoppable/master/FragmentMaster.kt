/*
 * Copyright (c) 2018 by civyshk
 * civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.project104.anstoppable.master

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import net.project104.anstoppable.*
import net.project104.anstoppable.SettingsActivity

import net.project104.anstoppable.db.TaskDao
import net.project104.anstoppable.db.DatabaseHelper
import net.project104.anstoppable.db.TaskMaster
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [FragmentMaster.Listener] interface
 * to handle interaction events.
 * Use the [FragmentMaster.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class FragmentMaster : Fragment() {
    private var listener: Listener? = null

    private lateinit var rootView: View
    private lateinit var taskAdapter: TaskAdapter
    private lateinit var dbHelper: DatabaseHelper
    private lateinit var dao: TaskDao

    var fab: FloatingActionButton? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        rootView = inflater.inflate(R.layout.fragment_master, container, false)
        dbHelper = (activity!!.application as MyApp).dbHelper
        dao = (activity!!.application as MyApp).taskDao

        val rvTasks = rootView.findViewById<RecyclerView>(R.id.rvMasterTasks)
        rvTasks.layoutManager = LinearLayoutManager(activity!!)
        rvTasks.itemAnimator = DefaultItemAnimator()

        //TODO this can be slow. Cache tasks and masterTasks and everything in database from start
        doAsync({ handleException(getClassTag(this)+".onCreateView", it) })
        {
            val tasks = dao.getAllTasks()

            val masterTasks = tasks.zip(0 until tasks.size)
                    .fold(mutableListOf<TaskMaster>()) { mTasks, pair->
                        if(dbHelper.isCounter(pair.first.id)) {
                            mTasks.apply { add(dbHelper.getCounterMerged(pair.first.id)) }
                        }else if(dbHelper.isClock(pair.first.id)){
                            mTasks.apply { add(dbHelper.getClockMerged(pair.first.id))}
                        }else{
                            throw RuntimeException("Error, found a task which is not a counter nor a clock")
                        }
                    }

            uiThread {
                taskAdapter = TaskAdapter(this@FragmentMaster.context!!, masterTasks, ::onClickTask, ::onDeleteTask)
                rvTasks.adapter = taskAdapter
            }
        }

        saveCurrentTaskInPreferences()

        return rootView
    }

    private fun saveCurrentTaskInPreferences() {
        val prefEditor = activity?.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE)?.edit()
        prefEditor?.putLong(PREF_LAST_TASK, -1L)
        prefEditor?.apply() ?: Log.d(getClassTag(this), "Tarea guardada ${-1}")
    }

    private fun onClickTask(taskId: Long, type: TaskType?) {
        listener?.onOpenTask(taskId, type)
    }

    private fun onDeleteTask(taskId: Long) {
        //TODO confirm dialog

        taskAdapter.removeTask(taskId)
        taskAdapter.notifyDataSetChanged()

        doAsync({ handleException(getClassTag(this) + ".onDeleteTask", it) }) {
            dao.deleteTask(taskId)
            uiThread {

            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val i = Intent()
        return when (item.itemId) {
            R.id.menu_settings -> {
                i.setClass(activity, SettingsActivity::class.java)
                i.putExtra("TaskType", -1)
                startActivity(i)
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        fab = activity!!.findViewById(R.id.fab)
//        fab?.layoutParams = (fab?.layoutParams as CoordinatorLayout.LayoutParams).apply {
//            gravity = Gravity.BOTTOM or Gravity.CENTER_HORIZONTAL
//        }
        fab?.visibility = View.VISIBLE

//        Debug.startMethodTracing("" + activity!!.getExternalFilesDir(null)!! + "trace");

        super.onResume()
    }

    override fun onPause() {
//        Debug.stopMethodTracing()
        super.onPause()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is Listener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface Listener {
        fun onOpenTask(taskId: Long, type: TaskType?)
        fun onDeleteTask(taskId: Long)
    }
/*
    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FragmentMaster.
         */
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                FragmentMaster().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }*/
}
