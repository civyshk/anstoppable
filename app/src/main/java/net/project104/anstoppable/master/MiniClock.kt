/*
 * Copyright (c) 2018 by civyshk
 * civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.project104.anstoppable.master

import android.content.res.Resources
import android.os.Handler
import android.os.Message
import android.widget.TextView
import net.project104.anstoppable.R
import net.project104.anstoppable.clock.HourFormat
import net.project104.anstoppable.clock.Mode
import net.project104.anstoppable.clock.SplitTime
import net.project104.anstoppable.db.Clock
import java.lang.ref.WeakReference
import java.text.NumberFormat
import kotlin.math.max

/**
 * This clock allows [FragmentMaster] and [TaskAdapter]
 * to update existing clocks in real time. Much code was
 * taken and simplified from [net.project104.anstoppable.clock.Clock],
 * where credits are given to other authors
 *
 * @param dbClock  Clock from database to take data from
 * @param tvTime  TextView to which write the time
 * @param res  Resources to localize time format
 */
class MiniClock(dbClock: Clock,
                internal var tvTime: TextView,
                internal val res: Resources) {

    var mode: Mode = Mode[dbClock.mode]

    var isStarted: Boolean = dbClock.isStarted

    /** Whether to show hours or not */
    var hourFormat: HourFormat = HourFormat[dbClock.hourFormat]

    /** Hour */
    var h: Int = 0

    /** Minute */
    var m: Int = 0

    /** Second */
    var s: Int = 0

    /** Decisecond, not shown in textview */
    var d: Int = 0

    /** 1-digit number formatter for hours */
    val nfH: NumberFormat = NumberFormat.getInstance().apply {
        minimumIntegerDigits = 1
    }

    /** 2-digit number format for minutes and seconds */
    val nfMS: NumberFormat = NumberFormat.getInstance().apply{
        minimumIntegerDigits = 2
    }

    /** Allows [MasterClocksThread] to update the view out of UI Thread */
    private val tvTimeHandler = TvTimeHandler(WeakReference<MiniClock>(this))

    init {
        initFields(dbClock)
        updateTextView()
    }

    fun reset(dbClock: Clock, tvTime: TextView) {
        this.tvTime = tvTime
        mode = Mode[dbClock.mode]
        isStarted = dbClock.isStarted
        hourFormat = HourFormat[dbClock.hourFormat]

        initFields(dbClock)
        updateTextView()
    }

    private fun initFields(dbClock: Clock) {
        val restoredAtTime = System.currentTimeMillis()
        val currentTime = when (mode) {
            Mode.COUNTDOWN -> dbClock.countdown * 1000L - if (isStarted)
                restoredAtTime - dbClock.startTimeAdj
            else
                dbClock.stopTime - dbClock.startTimeAdj

            Mode.STOP_LAP -> if (isStarted) restoredAtTime - dbClock.startTimeAdj else dbClock.stopTime - dbClock.startTimeAdj
        }

        val timeFields = splitTime(currentTime, hourFormat)
        h = timeFields.hour
        m = timeFields.min
        s = timeFields.sec
        d = timeFields.dsec
    }


    fun tickDecisecond(){
        if(!isStarted) return
        if(mode == Mode.STOP_LAP) incrementDecisecond()
        else decrementDecisecond()
    }

    private fun updateTextView(){
        tvTimeHandler.sendEmptyMessage(0)
    }

    private fun incrementDecisecond(){
        d++

        if (d == 10) {
            s++
            d = 0

            if (s == 60) {
                m++
                s = 0

                if (m >= 60 && hourFormat != HourFormat.MINUTES_PAST_60) {
                    h++
                    m -= 60
                }
            }

            updateTextView()
        }
    }

    private fun decrementDecisecond(){
        if (d == 0) {
            if (s == 0) {
                if (m == 0) {
                    if (h != 0) {
                        h--
                        m = 60
                    }
                }

                if (m != 0) {
                    m--
                    s = 60
                }
            }

            if (s != 0) {
                s--
                d = 10
            }
            updateTextView()
        }
        d--
    }

    /**
     * Split some amount of milliseconds into appropriate values of
     * hour, minute, second & decisecond. If [LapFormatter.hourFormat] is set to hide
     * hours, all hours will be summed as minutes
     * @param time Time in milliseconds
     * @param skipFormat If true, the method will always fill the hour field with
     *          its appropriate value independently of the setting in [LapFormatter.hourFormat]
     * @return A [SplitTime] object containing hour, minute, second & decisecond
     */
    private fun splitTime(time: Long, hourFormat: HourFormat) : SplitTime {
        val result = SplitTime()
        var total = max(0L, time)

        result.msec = (total % 10).toInt()
        total /= 10
        result.csec = (total % 10).toInt()
        total /= 10
        result.dsec = (total % 10).toInt()
        total /= 10
        result.sec = (total % 60).toInt()
        total /= 60
        if (hourFormat != HourFormat.MINUTES_PAST_60) {
            result.min = (total % 60).toInt()
            total /= 60
            result.hour = total.toInt()
        } else {
            result.min = total.toInt()
            result.hour = 0
        }
        return result
    }
}


private class TvTimeHandler(val miniClock: WeakReference<MiniClock>) : Handler() {
    override fun handleMessage(msg: Message) {
        val mc = miniClock.get() ?: return
        mc.tvTime.text = if (mc.h > 0 || mc.hourFormat == HourFormat.ALWAYS_SHOW) {
            mc.res.getString(R.string.lap_hour_no_dsec,
                    mc.nfH.format(mc.h),
                    mc.nfMS.format(mc.m),
                    mc.nfMS.format(mc.s))
        }else {
            mc.res.getString(R.string.lap_no_hour_no_dsec,
                    mc.nfMS.format(mc.m),
                    mc.nfMS.format(mc.s))
        }
    }
}