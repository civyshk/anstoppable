/*
 * Copyright (c) 2018 by civyshk
 * civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.project104.anstoppable.db

import net.project104.anstoppable.TaskType
import net.project104.anstoppable.clock.Mode

/**
 * Master classes are useful wrappers to counters and clocks
 * being used by [net.project104.anstoppable.master.TaskAdapter]
 * when showing them at [net.project104.anstoppable.master.FragmentMaster]
 */
abstract class TaskMaster(val taskId: Long,
                          val title: String){

    abstract val type: TaskType

    abstract fun getDetail() : String
}

class CounterMaster(taskId: Long,
                    title: String,
                    private val groups: List<Group>)
    : TaskMaster(taskId, title){

    override val type
        get() = TaskType.COUNTER

    override fun getDetail(): String
            = groups.map { it.groupCount } .joinToString(", ", limit=4)
}

class ClockMaster(val dbClock: Clock)
    : TaskMaster(dbClock.id, dbClock.title){

    override val type: TaskType
        get() = if(dbClock.mode == Mode.STOP_LAP.asInt) TaskType.STOPWATCH
                else TaskType.COUNTDOWN

    override fun getDetail(): String {
        throw RuntimeException("Dev, use MiniClock to show a clock detail")
    }
}

class DatabaseHelper(private val dao: TaskDao) {

    fun getCounterMerged(taskId: Long) : CounterMaster {
        val dbCounter = dao.getCounterByKey(taskId)
        val dbGroups = dao.getAllGroups(taskId)
        return CounterMaster(taskId, dbCounter.title, dbGroups)
    }

    fun getClockMerged(taskId: Long) : ClockMaster {
        val dbClock = dao.getClockByKey(taskId)
        val mode = if(isStopwatch(taskId)) Mode.STOP_LAP else Mode.COUNTDOWN
        return ClockMaster(dbClock)
    }

    //IMPROVE handle exceptions
    fun isCounter(taskId: Long) : Boolean {
        val cursor = dao.isCounter(taskId)
        cursor.moveToNext()
        return 1 == cursor.getInt(0)
    }

    fun isClock(taskId: Long) : Boolean {
        val cursor = dao.isClock(taskId)
        cursor.moveToNext()
        return 1 == cursor.getInt(0)
    }

    fun isStopwatch(taskId: Long) : Boolean {
        val cursor = dao.isStopwatch(taskId)
        cursor.moveToNext()
        return 1 == cursor.getInt(0)
    }
}