/*
 * Copyright (c) 2018 by civyshk
 * civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.project104.anstoppable.db

import android.arch.persistence.room.*
import android.arch.persistence.room.ForeignKey.CASCADE
import android.database.Cursor
import net.project104.anstoppable.clock.LapFormatter.Companion.LAP_FMT_FLAG_ELAPSED


/**
 * [Task], root class for every thing that a user can do
 */
@Entity
data class Task(@PrimaryKey(autoGenerate = true)
                var id: Long = 0)


/**
 * [Counter], to count things. 1, 2, 3, 1000000
 */
@Entity(foreignKeys = [ForeignKey(
        entity = Task::class,
        parentColumns = ["id"],
        childColumns = ["id"],
        onDelete = CASCADE)])
data class Counter(@PrimaryKey var id: Long = 0,
                   var title: String = "")


/**
 * Each [Counter] can have one or several [Group]s,
 * to count different things
 */
@Entity(tableName = "CounterGroup",

        primaryKeys = arrayOf(
                "id", "groupIndex"
        ),

        foreignKeys = arrayOf(ForeignKey(
            entity = Counter::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("id"),
            onDelete = CASCADE)))
data class Group(var id: Long,
                 var groupIndex: Int = 0,
                 var title: String = "",
                 var groupCount: Int = 0)


/**
 * Each [Counter] can have one or several [Button]s,
 * to count things faster than +1, +1, +1...
 * You better use a big button to sum +100
 */
@Entity(primaryKeys = ["id", "buttonIndex"],
        foreignKeys = [ForeignKey(
            entity = Counter::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("id"),
            onDelete = CASCADE)])
data class Button(var id: Long = 0,
                  var buttonIndex: Int = 0,
                  var value: Int = 1)


/**
 * With a [Clock] you can time the duration of events
 * or do a countdown from any time
 */
@Entity(foreignKeys = [ForeignKey(
            entity = Task::class,
            parentColumns = ["id"],
            childColumns = ["id"],
            onDelete = CASCADE)])
data class Clock(@PrimaryKey var id: Long = 0,
                 var mode: Int = 0, // mode 0 -> Stopwatch,  mode 1 -> Countdown. This must match those at enum Mode
                 var isStarted: Boolean = false,
                 var wasStarted: Boolean = false,
                 var startTimeActual: Long = -1,
                 var startTimeAdj: Long = -1,
                 var stopTime: Long = -1,
                 var countdown: Int = 0, // Only available for mode=Countdown
                 var comment: String = "",
                 var title: String = "",
                 var hourFormat: Int = 0, // HIDE_IF_ZERO. Values must match those at enum HourFormat
                 var numberDecimals: Int = 1,
                 var lapFormatFlags: Int = LAP_FMT_FLAG_ELAPSED
                 )


/**
 * Each [Clock] with mode=0 may have zero or more [Lap]s
 * which save partial times without stopping the clock
 */
@Entity(primaryKeys = ["id", "lapIndex"],
        foreignKeys = [(ForeignKey(
                entity = Clock::class,
                parentColumns = ["id"],
                childColumns = ["id"],
                onDelete = CASCADE))])
data class Lap(var id: Long = 0,
               var lapIndex: Int = 0,
               var elapsed: Long = 0,
               var systime: Long = -1)





@Dao
interface TaskDao{

    //Task
    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertTask(task: Task): Long

    @Query("DELETE FROM Task")
    fun clearWholeDatabase()

    @Query("SELECT COUNT(*)>0 FROM Counter WHERE id = :taskId")
    fun isCounter(taskId: Long) : Cursor

    @Query("SELECT COUNT(*)>0 FROM Clock WHERE id = :taskId")
    fun isClock(taskId: Long) : Cursor

    @Query("SELECT COUNT(*)>0 FROM Clock WHERE id = :taskId AND mode = 0")
    fun isStopwatch(taskId: Long) : Cursor

    @Query("SELECT * FROM Task")
    fun getAllTasks() : List<Task>

    @Query("DELETE FROM Task WHERE id = :taskId")
    fun deleteTask(taskId: Long)

    //Counter

    @Query("SELECT * FROM Counter")
    fun getAllCounters() : List<Counter>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCounter(counter: Counter):Long

    @Query("SELECT * FROM Counter where id = :id")
    fun getCounterByKey(id: Long): Counter

    @Update
    fun updateCounter(counter: Counter)

    @Delete
    fun deleteCounter(counter: Counter)

    //Group

    @Query("SELECT * FROM CounterGroup where id = :id")
    fun getAllGroups(id: Long) : List<Group>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertGroup(group: Group):Long

    @Query("SELECT * FROM CounterGroup where id = :id and groupIndex = :groupIndex")
    fun getGroupByKey(id: Long, groupIndex: Int): Group

    @Update
    fun updateGroup(group: Group)

    @Delete
    fun deleteGroup(group: Group)

    //Button

    @Query("SELECT * FROM Button where id = :id")
    fun getAllButtons(id: Long) : List<Button>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertButton(button: Button):Long

    @Query("SELECT * FROM Button where id = :id and buttonIndex = :buttonIndex")
    fun getButtonByKey(id: Long, buttonIndex: Int): Button

    @Update
    fun updateButton(button: Button)

    @Delete
    fun deleteButton(button: Button)
    
    //Clock
    
    @Query("SELECT * FROM Clock WHERE id = :id")
    fun getAllClocks(id: Long) : List<Clock>
    
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertClock(clock: Clock) : Long
    
    @Query("SELECT * FROM Clock WHERE id = :id")
    fun getClockByKey(id: Long) : Clock
    
    @Update
    fun updateClock(clock: Clock)

    @Delete
    fun deleteClock(clock: Clock)

    //Lap

    @Query("SELECT * FROM Lap where id = :id")
    fun getAllLaps(id: Long) : List<Lap>

    @Query("DELETE FROM Lap where id = :id")
    fun clearLaps(id: Long)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLap(lap: Lap):Long

    @Query("SELECT * FROM Lap where id = :id and lapIndex = :lapIndex")
    fun getLapByKey(id: Long, lapIndex: Int): Lap

    @Update
    fun updateLap(lap: Lap)

    @Delete
    fun deleteLap(lap: Lap)
    
}


@Database(entities = [
    Task::class,
    Counter::class,
    Group::class,
    Button::class,
    Clock::class,
    Lap::class], version = 3)
abstract class MyDatabase : RoomDatabase() {
    abstract fun taskDao() : TaskDao
}
