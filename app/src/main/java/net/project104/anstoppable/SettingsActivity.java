/*
 * Copyright (c) 2018 by civyshk
 * civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


package net.project104.anstoppable;


import android.os.Bundle;

import net.project104.anstoppable.clock.ThemeManager;

import static net.project104.anstoppable.MyAppKt.*;

public class SettingsActivity
	extends ThemedActivity
	implements ThemeManager.Listener {

    public SettingsActivity(){
		super(R.style.AnstopThemeLight_NoActionBar,
			  R.style.AnstopThemeDark_NoActionBar);
	}

    @Override
	public void onCreate(Bundle savedInstanceState) {
		logd(this, "onCreate()");
		super.onCreate(savedInstanceState);

		int taskTypeInt = getIntent().getIntExtra("TaskType", -2);
		TaskType taskType = TaskType.Companion.get(taskTypeInt);

		getSupportFragmentManager()
				.beginTransaction()
				.replace(android.R.id.content, new SettingsFragment(taskType))
				.commit();
	}


	@Override
	public void themeChanged() {
		recreate();

		// Alternative to recreate()
//		finish();
//		startActivity(new Intent(this, SettingsActivity.class));
	}


}
