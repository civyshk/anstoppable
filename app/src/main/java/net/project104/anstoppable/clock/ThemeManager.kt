/*
 * Copyright (c) 2018 by civyshk
 * civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.project104.anstoppable.clock

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import net.project104.anstoppable.PREF_DARK_THEME
import net.project104.anstoppable.logd

import java.lang.ref.WeakReference


/**
 * Activities can use an instance of this class, usually created in Application,
 * to easily switch between light and dark themes.
 * The preference is taken from SharedPreferences *
 *
 *
 * The themed Activity should, in its onCreate():
 *
 *  * Get a ThemeManager from Application
 *  * load the appropriate theme from using ThemeManager.isDarkTheme
 *  * call super.onCreate()
 *  * add itself as a listener
 *
 * In its onDestroy(): removeListener(Activity)
 *
 *
 * If using an AppCompatActivity with a support action bar,
 * you should probably call the onCreate code before
 * calling setSupportActionBar() to make sure that the chosen
 * theme doesn't have a conflicting toolbar.
 *
 *
 * If your activity inherits AppCompatActivity, the activity's theme must be
 * a descendant of Theme.AppCompat before even calling super.onCreate(),
 * so the ThemeManager should be set also before super.onCreate(). The
 * <application> tag in AndroidManifest.xml could specify a AppCompat theme
 * but I've found that the theme switching might not work well in that
 * scenery: background remains unchanged no matter the dark/light setting.
 *
 */
class ThemeManager(context: Context) : SharedPreferences.OnSharedPreferenceChangeListener {

    interface Listener {
        fun themeChanged()
    }

    private val listeners = mutableListOf<WeakReference<Listener>>()

    private val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            .apply {
                registerOnSharedPreferenceChangeListener(this@ThemeManager)
            }

    /** Style preference for dark or light themes */
    var isDarkTheme = preferences.getBoolean(PREF_DARK_THEME, false)

    /** Add a listener to be notified when the theme changes */
    fun addListener(listener: Listener){
        logd("addListener(); ${listener.hashCode()%1000}")
        listeners.add(WeakReference(listener))
    }

    /** Remove a listener */
    fun removeListener(listener: Listener) {
        logd("removeListener(); size=${listeners.size} L=${listener.hashCode()%1000}")
        listeners.removeAll {
            it.get() === listener
        }
        logd("removeListener(); size=${listeners.size}")
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        if (key == PREF_DARK_THEME) {
            isDarkTheme = preferences.getBoolean(PREF_DARK_THEME, false) == true

            listeners.forEach { it.get()?.themeChanged() }
        }
    }

}
