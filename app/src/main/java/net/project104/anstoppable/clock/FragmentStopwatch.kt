/*
 * Copyright (C) 2009-2011 by mj
 *   fakeacc.mj@gmail.com
 * Portions of this file Copyright (C) 2010-2012,2014-2016 Jeremy Monin
 *   jeremy@nand.net
 * Copyright (C) 2018 by civyshk
 *   civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.project104.anstoppable.clock


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ScrollView
import android.widget.TextView
import net.project104.anstoppable.R


class FragmentStopwatch() : FragmentClock() {

    override var mode: Mode = Mode.STOP_LAP

    internal lateinit var startButton: Button
    internal lateinit var resetButton: Button
    internal lateinit var lapButton: Button
    internal lateinit var lapView: TextView
    internal lateinit var lapScrollView: ScrollView

    /**
     * Fragment constructor arguments are better passed within
     * a Bundle. Use this to mimic that
     */
    @SuppressLint("ValidFragment")
    constructor (taskId: Long) : this(){
        arguments = Bundle().apply {
            putLong("TaskId", taskId)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    /**
     * Set the layout to [Mode.STOP_LAP], and reset clock to 0:0:0.0.
     * inform clock class to count laps now (same clock-action as STOP).
     */
    override fun setupViews() {
        super.setupViews()

        lapsContent.append(getString(R.string.laps))

        startButton = rootView.findViewById(R.id.startButton)
        resetButton = rootView.findViewById(R.id.resetButton)
        lapButton = rootView.findViewById(R.id.lapButton)

        startButton.setOnClickListener(StartButtonListener(Mode.STOP_LAP))
        resetButton.setOnClickListener(ResetButtonListener())
        lapButton.setOnClickListener(LapButtonListener())

        lapView = rootView.findViewById(R.id.lapView)
        lapScrollView = rootView.findViewById(R.id.lapScrollView)

        updateHourVisibility()
        updateDsecVisibility()
    }

    override fun getLayoutId(): Int = R.layout.fragment_stoplap

    override fun currentModeAsString(): String
            = getString(R.string.stopwatch)

    override fun createBodyFromCurrent(): String {
        // Start time, comment, and laps are all
        // within clockInfoView's or LapView's text.
        // The same formatting is used in updateStartTimeCommentLapsView
        // and AnstopDbAdapter.getRowAndFormat. If you change this, change those to match.
        // Code is not shared because this method doesn't need to re-read
        // the laps or reformat them.

        val hoursStr: String
        if (clock.hour != 0 || clock.lapFormatter.hourFormat == HourFormat.ALWAYS_SHOW)
            hoursStr = " ${hourView.text}"
        else
            hoursStr = ""

        return (getString(R.string.mode_was) + " "
                    + getString(R.string.stopwatch)
                    + hoursStr
                    + "\n" + minView.text.toString() + ":" + secondsView.text.toString()
                    + ":" + decimalsView.text.toString()
                    + "\n" + lapView.text.toString())

    }

    override fun resetClockAndViews() {
        if (clock.isStarted) {
            return
        }

        super.resetClockAndViews()

        lapView.text = ""
        clockInfoView.text = getClockInfoText()
    }

    /**
     * Format and write the start time, [comment], and [lapsContent] displayed
     * in [clockInfoView] or [lapView].
     * <P>
     * The same formatting is used in [AnstopDbAdapter.getRowAndFormat].
     * If you change this method, change that one to match.
     * <P>
     * [createBodyFromCurrent] also uses the same format;
     * code is not shared because that method doesn't need to re-read
     * the laps or reformat them.
     *
     * @param writeLaps  True if the hour counter format or lap format flags have changed
     */
    override fun updateStartTimeCommentLapsView(writeLaps: Boolean) {
        clockInfoView.text = getClockInfoText()

        if (writeLaps) {
            lapsContent.setLength(0)  // clear previous contents
            clock.lapFormatter.formatTimeAllLaps(lapsContent, clock.laps, resources)

            if (lapsContent.isNotEmpty()) {
                lapsContent.insert(0, getString(R.string.laps))
                lapView.text = lapsContent
            }
        }
    }

    override fun getDefaultClockTitle(): String = getString(R.string.stopwatch)

    private inner class LapButtonListener : View.OnClickListener {

        /** lap time for [onClick]; is empty between uses  */
        private val sb = StringBuilder()

        /**
         * Lap button clicked; get clock time from [Clock.addLap],
         * append it to [lapsContent] and [lapView].
         */
        override fun onClick(v: View) {
            val wasStarted = clock.wasStarted  // get value before clock.lap()

            if (lapsContent.isEmpty()) {
                lapsContent.append(R.string.laps)
                lapView.append(getString(R.string.laps))
            }
            sb.append("\n")
            clock.addLap(sb, resources)

            if (!(wasStarted || clock.isStarted)) {
                updateStartTimeCommentLapsView(false)
            }
            lapsContent.append(sb)
            lapView.append(sb)

            vibrateShort()

            // clear sb for the next onClick
            sb.setLength(0)

            // Scroll to bottom of lap times
            lapScrollView.post { lapScrollView.fullScroll(ScrollView.FOCUS_DOWN) }

            val lap = SimpleLap(clock.laps.last().systime, clock.laps.last().elapsed)

            // Record new lap in the db
//            dbHelper.createNewLap(0, lap.elapsed, lap.systime)

            // Back up new lap to viewmodel & database
            clockVM.addLap(lap)
            clockVM.saveClock(clock)
        }
    }
}
