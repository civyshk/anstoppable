/*
 * Copyright (C) 2009-2011 by mj
 *   fakeacc.mj@gmail.com
 * Portions of this file Copyright (C) 2010-2012,2014-2016 Jeremy Monin
 *   jeremy@nand.net
 * Copyright (C) 2018 by civyshk
 *   civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.project104.anstoppable.clock

import android.content.res.Resources
import net.project104.anstoppable.R
import net.project104.anstoppable.clock.Clock.Companion.mergeDecimals
import java.text.DateFormat
import java.text.DecimalFormat
import java.text.NumberFormat


/**
 * Flags, settings, and methods to format hours and laps.
 * Each [Clock] has one <tt>LapFormatter</tt>.
 * Used by [Clock.addLap]
 * and by [AnstopDbAdapter.getRowAndFormat].
 * The currently active flags are [lapFormatFlags].
 * By default, the formatter flags are [Clock.LAP_FMT_FLAG_ELAPSED] only.
 * The currently active hour format is [hourFormat].
 */
class LapFormatter() {

    /**
     * The active hour format; default is [HourFormat.HIDE_IF_0].
     * Read-only from [FragmentClock] class. To change, call [Clock.setHourFormat].
     * But AnstopDbAdapter access it to, so leave it public
     * Is used to control whether [Clock.min] wraps around at 60, whether hours are a field
     * in [formatTimeLap],
     * and anywhere else hours and minutes are formatted or shown.
     * @since 1.6
     */
    var hourFormat: HourFormat = HourFormat.HIDE_IF_0
//            internal set


    /**
     * Show this number of decimal positions after seconds
     */
    var numberDecimals: Int = 3
        set(value) {
            field = value
            nfD.minimumIntegerDigits = value
        }

    /**
     * Any lap format flags, such as [Clock.LAP_FMT_FLAG_SYSTIME], currently
     * active; the default is [Clock.LAP_FMT_FLAG_ELAPSED] only.
     * Read-only from [FragmentClock] class.
     * To change, call [setLapFormat].
     * <P>
     * **Note:** Currently, code and fragment_clock_settings.xml both assume that
     * the default format has LAP_FMT_FLAG_ELAPSED and no others,
     * unless the user changes that preference.
     */
    var lapFormatFlags = LAP_FMT_FLAG_ELAPSED

    /**
     * Time-of-day format used in [Clock.appendLap]
     * for lap format, when [Clock.LAP_FMT_FLAG_SYSTIME] is used.
     * <P>
     * This is null initially; [Clock.LAP_FMT_FLAG_ELAPSED] doesn't need it.
     * If [setLapFormat] changes [lapFormatFlags] to
     * anything else, it's a required parameter, so it would be non-null when needed.
     */
    private var lapFormatTimeOfDay: java.text.DateFormat? = null

    /**
     * 1-digit number formatter for hours
     */
    var nfH: NumberFormat = NumberFormat.getInstance().apply {
        minimumIntegerDigits = 1
        maximumIntegerDigits = 9
    }

    /**
     * 2-digit number format for minutes and seconds;
     * <tt>[NumberFormat.format](3)</tt> gives "03".
     */
    var nfMS: NumberFormat = NumberFormat.getInstance().apply {
        minimumIntegerDigits = 2
        maximumIntegerDigits = 9
    }

    /**
     * Variable digit number formatter for decimals of second
     */
    var nfD: NumberFormat = NumberFormat.getInstance().apply {
        minimumIntegerDigits = numberDecimals // This is variable
        maximumIntegerDigits = 3 // 3 is enough for milliseconds (max allowed precision)
    }

    /**
     * Separate seconds from decimal fraction of seconds
     * using this localized character
     */
    val decimalSeparator: Char =
            (NumberFormat.getInstance() as? DecimalFormat)?.decimalFormatSymbols?.decimalSeparator ?: '.'


    /**
     * Set the lap format flags.
     * @param newFormatFlags  Collection of flags, such as [Clock.LAP_FMT_FLAG_DELTA]; not 0
     * @param formatForSysTime Short time format in case [Clock.LAP_FMT_FLAG_SYSTIME] is used; not null.
     * Value should be <tt>android.text.format.DateFormat.getTimeFormat(getApplicationContext())</tt>.
     * Note that <tt>getTimeFormat</tt> gives hours and minutes, it has no standard way to include the seconds;
     * if more precision is needed, the user can get it from elapsed or delta seconds.
     * @throws IllegalArgumentException if flags &lt;= 0, or <tt>formatForSysTime == null</tt>
     * @see Clock.setHourFormat
     */
    @Throws(IllegalArgumentException::class)
    fun setLapFormat(newFormatFlags: Int, formatForSysTime: DateFormat?) {
        if (newFormatFlags <= 0 || formatForSysTime == null) {
            throw IllegalArgumentException()
        }
        lapFormatFlags = newFormatFlags
        lapFormatTimeOfDay = formatForSysTime
    }

    /**
     * Format one lap's time, appending it to a buffer.
     * @param sb   Buffer to append to; non null
     * @param withLap  If true, append <tt>lapNum</tt>
     * @param h        Elapsed hours if known, or -1 to calculate from <tt>elapsedMillis</tt>;
     * used with flag [Clock.LAP_FMT_FLAG_ELAPSED]
     * @param m   Elapsed minutes if known; if <tt>h</tt> is -1, will calculate from <tt>elapsedMillis</tt>
     * @param s   Elapsed seconds if known; if <tt>h</tt> is -1, will calculate from <tt>elapsedMillis</tt>
     * @param ds  Elapsed deciseconds if known; if <tt>h</tt> is -1, will calculate from <tt>elapsedMillis</tt>
     * @param cs  Elapsed centiseconds if known; if <tt>h</tt> is -1, will calculate from <tt>elapsedMillis</tt>
     * @param ms  Elapsed milliseconds if known; if <tt>h</tt> is -1, will calculate from <tt>elapsedMillis</tt>
     * @param lapNum   Lap number
     * @param elapsedMillis  This lap's milliseconds representing h, m, s, ds
     * @param systimeMillis  This lap's system time; [System.currentTimeMillis]
     * @param laps  Array of previous laps' elapsed times;
     * used with flag [Clock.LAP_FMT_FLAG_DELTA]
     * @param res  Resources to localize time format
     */
    fun formatTimeLap(sb: StringBuilder, withLap: Boolean,
                      h: Int, m: Int, s: Int, ds: Int, cs: Int, ms: Int,
                      lapNum: Int, elapsedMillis: Long, systimeMillis: Long,
                      laps: List<SimpleLap>?, res: Resources) {

        var h = h
        var m = m
        var s = s
        var ds = ds
        var cs = cs
        var ms = ms

        var sbNeedsSpace = false  // true if appended anything that needs a space afterwards

        val hourFmt = hourFormat

        if (withLap) {
            sb.append(res.getString(R.string.lap_prefix ,lapNum + 1))
        }

        if (0 != lapFormatFlags and LAP_FMT_FLAG_ELAPSED) {
            if (h == -1) {

                var elapsed = elapsedMillis
                ms = (elapsed % 10).toInt()
                elapsed /= 10
                cs = (elapsed % 10).toInt()
                elapsed /= 10
                ds = (elapsed % 10).toInt()
                elapsed /= 10
                s = (elapsed % 60).toInt()
                elapsed /= 60L
                if (hourFmt != HourFormat.MINUTES_PAST_60) {
                    m = (elapsed % 60).toInt()
                    elapsed /= 60L
                    h = elapsed.toInt()
                } else {
                    m = elapsed.toInt()
                    h = 0
                }
            }

            if (h > 0 && hourFmt == HourFormat.MINUTES_PAST_60) {
                m += 60 * h
                h = 0
            }

            sb.append(formatTime(h.toLong(), m.toLong(), s.toLong(), ds.toLong(), cs.toLong(), ms.toLong(), hourFormat, res))
            sbNeedsSpace = true
        }

        if (0 != lapFormatFlags and LAP_FMT_FLAG_DELTA) {
            val prevLap = if (lapNum > 0) laps!![lapNum - 1].elapsed else 0
            var lapDelta = (elapsedMillis - prevLap)
            val dms = (lapDelta % 10)
            lapDelta /= 10
            val dcs = lapDelta % 10
            lapDelta /= 10
            val dds = (lapDelta % 10)
            lapDelta /= 10
            val dsec = (lapDelta % 60)
            lapDelta /= 60
            val dm: Long
            if (hourFmt != HourFormat.MINUTES_PAST_60) {
                dm = (lapDelta % 60)
                lapDelta /= 60
            } else {
                dm = lapDelta
                lapDelta = 0
            }

            if (sbNeedsSpace) sb.append(' ')

            sb.append("(+")
            sb.append(formatTime(lapDelta, dm, dsec, dds, dcs, dms, hourFormat, res))
            sb.append(')')
            sbNeedsSpace = true
        }

        if (0 != lapFormatFlags and LAP_FMT_FLAG_SYSTIME && lapFormatTimeOfDay != null) {
            if (sbNeedsSpace) sb.append(' ')

            sb.append('@')
            sb.append(lapFormatTimeOfDay!!.format(systimeMillis))
        }
    }

    private fun formatTime(h: Long, m: Long, s: Long, ds: Long, cs: Long, ms: Long,
                           hourFormat: HourFormat, res: Resources)
            : String =

        if (h > 0 || hourFormat == HourFormat.ALWAYS_SHOW) {
            if(numberDecimals > 0) {
                res.getString(R.string.lap_hour,
                        nfH.format(h),
                        nfMS.format(m),
                        nfMS.format(s),
                        nfD.format(mergeDecimals(ds.toInt(), cs.toInt(), ms.toInt(), numberDecimals)))
            }else{
                res.getString(R.string.lap_hour_no_dsec,
                        nfH.format(h),
                        nfMS.format(m),
                        nfMS.format(s))
            }
        }else{
            if(numberDecimals > 0) {
                res.getString(R.string.lap_no_hour,
                        nfMS.format(m),
                        nfMS.format(s),
                        nfD.format(mergeDecimals(ds.toInt(), cs.toInt(), ms.toInt(), numberDecimals)))
            }else{
                res.getString(R.string.lap_no_hour_no_dsec,
                        nfMS.format(m),
                        nfMS.format(s))
            }
    }

    /**
     * Write all laps into <tt>sb</tt> using the current format flags.
     * If <tt>laps</tt> is empty, do nothing.
     * @param sb  StringBuffer to write into; not null
     * @param laps  List of laps
     * @throws IllegalArgumentException if <tt>sb</tt> null
     */
    @Throws(IllegalArgumentException::class)
    fun formatTimeAllLaps(sb: StringBuilder, laps: List<SimpleLap>?, res: Resources) {
        if (laps == null) return

        for (i in laps.indices) {
            sb.append('\n')
            formatTimeLap(sb, true, -1, 0, 0, 0, 0, 0, i,
                    laps[i].elapsed, laps[i].systime, laps, res)
        }
    }

    /**
     * Restore fields from database
     * @param dbClock  clock entry from database
     */
    fun restoreFromDatabase(dbClock: DBClock) {
        hourFormat = HourFormat[dbClock.hourFormat]
        lapFormatFlags = dbClock.lapFormatFlags
        numberDecimals = dbClock.numberDecimals
    }

    /**
     * Save important fields into [DBClock]
     * @param dbClock  Object into which save data
     */
    fun fillClockDB(dbClock: DBClock) {
        dbClock.hourFormat = hourFormat.asInt
        dbClock.lapFormatFlags = lapFormatFlags
        dbClock.numberDecimals = numberDecimals
    }

    companion object {

        // Note [Not checked] : Currently, code and fragment_clock_settings.xml both assume that
        //   the default format has LAP_FMT_FLAG_ELAPSED and no others,
        //   unless the user changes that preference.

        /** Lap time format flag: Elapsed. <tt>h mm:ss:d</tt>  */
        const val LAP_FMT_FLAG_ELAPSED = 1

        /** Lap time format flag: Delta. <tt>(+h mm:ss:d)</tt>  */
        const val LAP_FMT_FLAG_DELTA = 2

        /** Lap time format flag: System time. <tt>@hh:mm</tt>  */
        const val LAP_FMT_FLAG_SYSTIME = 4
    }
}