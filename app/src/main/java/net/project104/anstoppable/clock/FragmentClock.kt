/*
 * Copyright (C) 2009-2011 by mj
 *   fakeacc.mj@gmail.com
 * Portions of this file Copyright (C) 2010-2012,2014-2016 Jeremy Monin
 *   jeremy@nand.net
 * Copyright (C) 2018 by civyshk
 *   civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


package net.project104.anstoppable.clock

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Vibrator
import android.preference.PreferenceManager
import android.text.format.DateFormat
import android.view.View.OnClickListener
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

import android.hardware.SensorManager
import android.os.Build
import android.os.VibrationEffect
import android.support.design.widget.Snackbar
import android.text.ClipboardManager
import android.view.*
import android.widget.Button
import net.project104.anstoppable.*
import org.jetbrains.anko.defaultSharedPreferences
import kotlin.math.min

abstract class FragmentClock : FragmentTask() {

    // Reminder: If you add or change fields, be sure to update
    // [Clock.restoreFromDatabase] and [Clock.fillClockDB]

    lateinit var clockVM: ClockViewModel

    internal lateinit var dbHelper: AnstopDbAdapter

    abstract var mode: Mode

    /**
     * Date formatter for day of week + user's medium date format + hh:mm:ss;
     * used in [updateStartTimeCommentLapsView] for "started at:".
     */
    private var fmt_dow_meddate_time: StringBuffer? = null

    /** Date formatter for just hh:mm:ss; used in [addDebugLog].  */
    private var fmt_debuglog_time: StringBuffer? = null

    internal lateinit var clock: Clock

    /** Lap data for [lapView].  */
    internal var lapsContent: StringBuilder = StringBuilder()

    /**
     * Optional comment, or null.
     * In the layout, Start time, <tt>comment</tt>, and [lapsContent]
     * are all shown in clockInfoView or LapView.
     * @see [updateStartTimeCommentLapsView]
     */
    internal var comment: String = ""

    internal var title: String = ""

    /** root view of the fragment */
    internal lateinit var rootView: ViewGroup

    internal lateinit var btnStartStop: Button
    internal lateinit var clockInfoView: TextView

    internal lateinit var hourView: TextView
    internal lateinit var minView: TextView
    internal lateinit var secondsView: TextView
    internal lateinit var decimalsView: TextView
    internal lateinit var tvDecimalSeparator: TextView

    /**
     * This fake (invisible) view should have a width slightly
     * bigger than the normal clock 00:00:00
     * @see clock.xml in res/layout for more info
     */
    private lateinit var fakeClockView: TextView

    /** Context menu item for Save. Null until [onCreateOptionsMenu] is called.  */
    //TODO find usages and uncomment them
//    internal lateinit var saveMenuItem: MenuItem

    /**
     * Not null if the app should vibrate (quick pulse) with button presses.
     * (`vibrate` preference setting)
     * <P>
     * Not used by other vibration preferences such as `vibrate_countdown_0`.
     */
    internal var vib: Vibrator? = null

    internal var al: AccelerometerListener? = null

    private val debugLog = StringBuilder()

    /**
     * Optional notes from Debug Log dialog.
     * @since 1.6
     */
    private val debugNotes = StringBuilder()


    /**
     * Called when the fragment is first created.
     * Assumes [Mode.STOP_LAP] mode and sets that layout.
     * Preferences are read, which calls setCurrentMode.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        logd("onCreate()")
        super.onCreate(savedInstanceState)

        clockVM = ViewModelProviders.of(this).get(ClockViewModel::class.java)

        //Try to get desired taskId from activity arguments or savedInstanceState
        taskId = arguments?.getLong("TaskId", -1L) ?: (savedInstanceState?.getLong("TaskId", -1L)
                ?: -1L)

        //If none, maybe clockVM saved its state
        if (taskId == -1L) {
            taskId = clockVM.taskId
        }

        // Set the default shared pref values, especially for checkboxes.
        // See android bug http://code.google.com/p/android/issues/detail?id=6641
        PreferenceManager.setDefaultValues(activity, R.xml.settings, true)

        clock = Clock(this)

        // Decide whether to init a new Clock or load a previous one
        if (taskId == -1L) {
            clockVM.initNew(mode) {
                taskId = clockVM.taskId
                saveCurrentTaskInPreferences()
            }
        } else {
            clockVM.initId(taskId) {
                taskId = clockVM.taskId
                saveCurrentTaskInPreferences()
            }
        }

        dbHelper = (activity!!.application as MyApp).anstopDbHelper

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        logd("onCreateView()")
        super.onCreateView(inflater, container, savedInstanceState)

        rootView = inflater.inflate(getLayoutId(), container, false) as ViewGroup

        // set Views Buttons and Listeners for the new Layout;
        // set current and clock to STOP_LAP mode at 00:00:00.0
        setupViews()

        //read Preferences
        readSettings(true)

        clockVM.initialized.observe(this@FragmentClock,
                Observer<Boolean> { initialized ->
                    if (initialized!!) {
                        restoreClockAfterViewModel()
                        clockVM.initialized.removeObservers(this@FragmentClock)
                    }
                })

        return rootView
    }

    private fun restoreClockAfterViewModel() {
        logd("restoreClockAfterViewModel()")
        clock.changeMode(mode)
        clock.restoreFromDatabase(clockVM.clock, clockVM.laps, resources)
        logd("restoreClockAfterViewModel(); isStarted=${clock.isStarted}, wasStarted=${clock.wasStarted}")

        updateHourVisibility()
        updateDsecVisibility()
    }

    abstract fun getLayoutId(): Int

    private fun updatePreferencesWithClock() {
        val pref = activity!!.defaultSharedPreferences
        pref.edit()
                .putString(PREF_NUMBER_DECIMALS,
                        clock.lapFormatter.numberDecimals.toString())
                .putString(PREF_HOUR_FORMAT,
                        clock.lapFormatter.hourFormat.asInt.toString())
                .putBoolean(PREF_LAP_FORMAT_ELAPSED,
                        (clock.lapFormatter.lapFormatFlags and LapFormatter.LAP_FMT_FLAG_ELAPSED) != 0)
                .putBoolean(PREF_LAP_FORMAT_DELTA,
                        (clock.lapFormatter.lapFormatFlags and LapFormatter.LAP_FMT_FLAG_DELTA) != 0)
                .putBoolean(PREF_LAP_FORMAT_SYSTIME,
                        (clock.lapFormatter.lapFormatFlags and LapFormatter.LAP_FMT_FLAG_SYSTIME) != 0)
                .apply()
    }

    /**
     * Read our settings, at startup or after calling the SettingsActivity.
     * <P>
     * Does not check <tt>"anstop_in_use"</tt> or read the instance state
     * when the application is finishing; for that, see [Clock.restoreFromSaveState].
     *
     * @param isStartup Are we just starting now, not already running?
     */
    private fun readSettings(isStartup: Boolean) {

        val settings = PreferenceManager.getDefaultSharedPreferences(context)
        if (settings.getBoolean(PREF_USE_MOTION_SENSOR, false)) {
            al = AccelerometerListener(activity!!.getSystemService(Context.SENSOR_SERVICE) as SensorManager, clock)
            al!!.start()
        } else {
            al?.stop()
        }

        if (settings.getBoolean(PREF_VIBRATE, true))
            vib = activity!!.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        else
            vib = null

        // Hour Counter and Lap Display Format settings
        try {

            val settingHour = HourFormat[Integer.parseInt(settings.getString(PREF_HOUR_FORMAT, "0"))]  // default Clock.HOUR_FMT_HIDE_IF_0
            val settingDecimals = Integer.parseInt(settings.getString(PREF_NUMBER_DECIMALS, "1"))

            // read the flags, possibly just changed by user; default is LAP_FMT_FLAG_ELAPSED only
            var settingLap = readLapFormatPrefFlags(settings)
            if (settingLap == 0) {
                // Should not happen, but if it does, correct it to default
                settings.edit().putBoolean("lap_format_elapsed", true).apply()
                settingLap = LapFormatter.LAP_FMT_FLAG_ELAPSED
            }

            var needUpdate = false
            if (isStartup || settingHour != clock.lapFormatter.hourFormat) {
                needUpdate = true
                clock.setHourFormat(settingHour)
                updateHourVisibility()
            }

            if (isStartup || settingDecimals != clock.lapFormatter.numberDecimals) {
                needUpdate = true
                clock.lapFormatter.numberDecimals = min(settingDecimals, 3)
                updateDsecVisibility()
                if (!isStartup)
                    clock.updateTimeAndGui(force = true)
            }

            if (settingLap != clock.lapFormatter.lapFormatFlags) {
                clock.setLapFormat(settingLap, DateFormat.getTimeFormat(context))
                needUpdate = true
            }

            if (needUpdate)
                updateStartTimeCommentLapsView(true)

        } catch (e: Throwable) {
            loge("readSettings()", e)
        }
    }

    /**
     * Show or hide [hourView] and [hourLabelView] if needed,
     * based on hour != 0 and current [Clock.LapFormatter.hourFormat] setting.
     * @since 1.6
     */
    internal fun updateHourVisibility() {
        val hourWantVisible = clock.hour > 0 || clock.lapFormatter.hourFormat == HourFormat.ALWAYS_SHOW
        val hourVis = if (hourWantVisible) View.VISIBLE else View.GONE

        hourView.visibility = hourVis
    }

    /**
     * Show or hide [decimalsView] and [tvDecimalSeparator] if needed,
     * based on current [LapFormatter.numberDecimals] setting.
     */
    internal fun updateDsecVisibility() {
        val visible = if (clock.lapFormatter.numberDecimals > 0) View.VISIBLE else View.GONE
        decimalsView.visibility = visible
        tvDecimalSeparator.visibility = visible
        updateFakeClockWidth()
    }

    /**
     * stopwatch and countdown fragments must implement this because their
     * layouts are slightly different. They must call super.setupViews()
     */
    @SuppressLint("ClickableViewAccessibility")
    internal open fun setupViews() {
        btnStartStop = rootView.findViewById(R.id.startButton)
        clockInfoView = rootView.findViewById(R.id.startTimeView)

        hourView = rootView.findViewById(R.id.hourView)
        minView = rootView.findViewById(R.id.minView)
        secondsView = rootView.findViewById(R.id.secondsView)
        decimalsView = rootView.findViewById(R.id.decimalsView)
        tvDecimalSeparator = rootView.findViewById(R.id.tvDecimalSeparator)

        fakeClockView = rootView.findViewById<TextView>(R.id.tvFakeTime)
        updateFakeClockWidth()

        rootView.findViewById<TextView>(R.id.tvDecimalSeparator).text =
                clock.lapFormatter.decimalSeparator.toString()


        /*
         * The container [layControls] will listen to long clicks and
         * has probably set a ripple effect when touched.
         *
         * Inner views will interfere with those touch events, so
         * I catch them and deliver to outer container to enable ripple
         * effects on it.
         */

        setupCommentLongPress(rootView.findViewById(R.id.layControls))

        val layControls: ViewGroup = rootView.findViewById(R.id.layControls)

        // Nullables because Countdown doesn't have laps
        val lapScrollView: ViewGroup? = rootView.findViewById(R.id.lapScrollView)
        val layComments: ViewGroup? = rootView.findViewById(R.id.layComments)
        val lapView: TextView? = rootView.findViewById(R.id.lapView)

        clockInfoView.setOnTouchListener { v, e ->

            // MotionEvent coordinates need to be fixed before being delivered to layControls

            val shiftedEvent = MotionEvent.obtain(e.downTime, e.eventTime, e.action,
                    e.x + v.x + (layComments?.x ?: 0.0f) + (lapScrollView?.x ?: 0.0f),
                    e.y + v.y + (layComments?.y ?: 0.0f) + (lapScrollView?.y ?: 0.0f),
                    e.metaState)

            // Deliver event to layControls

            layControls.onTouchEvent(shiftedEvent)
        }

        lapView?.setOnTouchListener { v, e ->
            val shiftedEvent = MotionEvent.obtain(e.downTime, e.eventTime, e.action,
                    e.x + v.x + (layComments?.x ?: 0.0f) + (lapScrollView?.x ?: 0.0f),
                    e.y + v.y + (layComments?.y ?: 0.0f) + (lapScrollView?.y ?: 0.0f),
                    e.metaState)

            layControls.onTouchEvent(shiftedEvent)
        }

        lapScrollView?.setOnTouchListener { v, e ->
            val shiftedEvent = MotionEvent.obtain(e.downTime, e.eventTime, e.action,
                    e.x + v.x,
                    e.y + v.y,
                    e.metaState)

            layControls.onTouchEvent(shiftedEvent)
            false
        }
    }

    @SuppressLint("SetTextI18n")
    fun updateFakeClockWidth() {
        fakeClockView.text =
                clock.lapFormatter.nfMS.format(8) + "::" +
                clock.lapFormatter.nfMS.format(8) +
                if (clock.lapFormatter.numberDecimals > 0) "." + clock.lapFormatter.nfD.format(8)
                else ""
    }

    /**
     * Set up long-press on the read-only start time / lap textview
     * to allow editing the optional [comment].
     * @param tv  Either [lapView] or [clockInfoView]
     */
    internal fun setupCommentLongPress(tv: View?) {
        tv?.setOnLongClickListener {
            showDialog(COMMENT_DIALOG)
            true
        }

        if (tv == null) {
            logd("setupCommentLongPress(); es null")
        } else {
            logd("setupCommentLongPress(); long press ok")
        }
    }

    /**
     * Stop the clock thread.
     */
    override fun onPause() {
        logd("onPause()")
        super.onPause()
        clock.onAppPause()
    }

    override fun onResume() {
        logd("onResume(); isStarted=${clock.isStarted}, wasStarted=${clock.wasStarted}")
        super.onResume()

        updateHourVisibility()
        updateDsecVisibility()
        if (clock.isStarted) {
            clock.onAppResume()
            //This code could be not reached because clock.isStarted is set to true
            //after clockViewModel loads its data in another thread. When it finishes,
            //clock is started correctly even if this noAppResume is not executed
        }
    }

    /**
     * Add to or inflate the option menu for action bar or android 2.x menu key.
     * Inflates from `res/menu/menu_clock.xml.
     * Also sets [saveMenuItem].
     * <P>
     * Anstop doesn't need `onPrepareOptionsMenu(..)` because menu item state changes
     * are handled as they occur (start or stop clock, change mode, etc).
     *
     * @param menu  Add to or inflate into this menu
     * @param menuInflater  The menu inflater
     */
    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {

        menuInflater.inflate(R.menu.menu_clock, menu)

//        saveMenuItem = menu.findItem(R.id.menu_save)
//        saveMenuItem.isEnabled = !clock.isStarted

        // debug
        if (!DEBUG_LOG_ENABLED)
            menu.removeItem(R.id.menu_debug_log)

        super.onCreateOptionsMenu(menu, menuInflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val i = Intent()

        when (item.itemId) {
            R.id.menu_settings -> {
                i.setClass(activity, SettingsActivity::class.java)
                i.putExtra("TaskType", when (mode) {
                    Mode.STOP_LAP -> TaskType.STOPWATCH.asInt
                    Mode.COUNTDOWN -> TaskType.COUNTDOWN.asInt
                })
                updatePreferencesWithClock()
                startActivityForResult(i, SETTINGS_ACTIVITY)
                // on result, will call readSettings(false).
                return true
            }

            //TODO fix and enable
//            R.id.menu_save -> {
//                showDialog(SAVE_DIALOG)
//                return true
//            }
//
//            R.id.menu_send -> {
//                startSendMailIntent(activity!!, getString(R.string.app_name) + ": " + currentModeAsString(), createBodyFromCurrent())
//                return true
//            }
//
//            R.id.menu_load -> {
//                i.setClass(context, LoadActivity::class.java)
//                startActivity(i)
//                return true
//            }

            R.id.menu_debug_log -> {
                showDialog(DEBUG_LOG_DIALOG)
                return true
            }
        }

        return false
    }

    private fun showDialog(id: Int) {
        when (id) {
            SAVE_DIALOG -> showDialogSave()
            COMMENT_DIALOG -> showDialogComment()
            DEBUG_LOG_DIALOG -> showDialogDebugLog()
        }
    }

    private fun showDialogComment() {
        val clockDialog = ClockDialog(title, comment)
        clockDialog.setTargetFragment(this, COMMENT_DIALOG)
        clockDialog.show(activity!!.supportFragmentManager, "DialogComment")
    }

    private fun showDialogDebugLog() {
        val dldBuilder = AlertDialog.Builder(activity)

        // Include debug log text and optional notes textfield
        val v = layoutInflater.inflate(R.layout.anstop_debug_log, null)
        val tvText = v.findViewById(R.id.debugLogText) as TextView
        tvText.text = debugLog
        val etNotes = v.findViewById(R.id.debugLogNotes) as EditText
        etNotes.setText(debugNotes)

        dldBuilder.setView(v)
                .setTitle(R.string.debug_log)
                .setCancelable(true)
                .setNeutralButton(android.R.string.copy) { dialog, which ->

                    // use android.text.ClipboardManager for compat with API level < 11
                    val clipboard = activity!!.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                    var debugText: CharSequence = debugLog
                    debugNotes.setLength(0)

                    val notes = etNotes.text
                    if (notes.isNotEmpty()) {
                        debugNotes.append(notes)

                        val sb = StringBuilder(debugText)
                        sb.append("\n\n")
                        sb.append(notes)
                        debugText = sb
                    }
                    clipboard.text = debugText
                    Toast.makeText(activity, R.string.copied_to_clipboard, Toast.LENGTH_SHORT).show()
                }
                .setPositiveButton(android.R.string.ok) { dialog, id ->
                    debugNotes.setLength(0)
                    val notes = etNotes.text
                    if (notes.isNotEmpty())
                        debugNotes.append(notes)

                    dialog.dismiss()
                }

        val dialog: Dialog? = dldBuilder.create()
        dialog?.show()
    }

    private fun showDialogSave() {
        val saveBuilder = AlertDialog.Builder(activity)
        saveBuilder.setTitle(R.string.save)
        saveBuilder.setMessage(R.string.save_dialog)
        val input = EditText(activity)
        saveBuilder.setView(input)

        saveBuilder.setPositiveButton(android.R.string.ok) { dialog, whichButton ->
            val id = dbHelper.createNew(input.text.toString().trim { it <= ' ' }, comment,
                    clock.mode, clock.startTimeActual, clock.stopTime, clock.currentTime)
            if (clock.laps.isNotEmpty())
                dbHelper.createNewLaps(id, clock.laps)

            val toast = Toast.makeText(activity, R.string.saved_succes, Toast.LENGTH_SHORT)
            toast.show()
        }

        saveBuilder.setNegativeButton(R.string.cancel) { dialog, whichButton -> dialog.dismiss() }
        saveBuilder.setCancelable(false)
        val dialog: Dialog? = saveBuilder.create()
        dialog?.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        addDebugLog("onActivityResult")

        when (requestCode) {
            SETTINGS_ACTIVITY -> {
                readSettings(false)
                clockVM.saveClock(clock)
            }
            COMMENT_DIALOG -> {
                if (resultCode != Activity.RESULT_OK)
                    return

                comment = data?.getStringExtra(ClockDialog.DATA_CLOCK_COMMENT) ?: ""
                title = data?.getStringExtra(ClockDialog.DATA_CLOCK_TITLE) ?: ""

                updateStartTimeCommentLapsView(false)

                // Back up current state with new comments to viewmodel & database
                clockVM.saveClock(clock)
            }
        }
    }

    /**
     * Given the [Clock.mode], the name of that mode from resources.
     * @return mode name.
     */
    abstract fun currentModeAsString(): String

    /**
     * Construct a string with the current mode, time,
     * [comment], and [lapsContent] (if applicable).
     */
    internal abstract fun createBodyFromCurrent(): String

    /**
     * For stopwatch/lap mode,
     * Reset the clock and hh/mm/ss views.
     * Clear <tt>"anstop_in_use"</tt> flag in shared preferences.
     * <P>
     * If isStarted, do nothing.
     * If wasStarted, call this only after the confirmation alert.
     *
     * @see [refreshCountdownTime]
     */
    internal open fun resetClockAndViews() {
        if (clock.isStarted) {
            return
        }

        val anyLaps = clock.laps.isNotEmpty()
        clock.reset(null, 0, 0, 0)

        //reset all Views to zero
        decimalsView.text = clock.lapFormatter.nfD.format(0.toLong())
        secondsView.text = clock.lapFormatter.nfMS.format(0.toLong())
        minView.text = clock.lapFormatter.nfMS.format(0.toLong())
        hourView.text = resources.getQuantityString(R.plurals.hours_long, 0,
                clock.lapFormatter.nfH.format(0.toLong()))
        lapsContent.setLength(0)

        if (anyLaps) {
            dbHelper.deleteTemporaryLaps()
        }

        updateHourVisibility()

        // Clear any old anstop_in_use flags from previous runs
        val settings = PreferenceManager.getDefaultSharedPreferences(activity)
        if (settings.getBoolean("anstop_in_use", false)
                || settings.getBoolean("anstop_state_clockActive", false)
                || settings.getBoolean("anstop_state_clockWasActive", false)) {
            val outPref = settings.edit()
            outPref.putBoolean("anstop_in_use", false)
            outPref.putBoolean("anstop_state_clockActive", false)
            outPref.putBoolean("anstop_state_clockWasActive", false)
            outPref.apply()
        }

        clockVM.saveClock(clock)
        clockVM.clearLaps()
    }

    /**
     * Format and write the start time, [comment],
     * and [lapsContent] (if applicable) displayed
     * in [clockInfoView] or [lapView].
     * <P>
     * The same formatting is used in [AnstopDbAdapter.getRowAndFormat].
     * If you change this method, change that one to match.
     * <P>
     * [createBodyFromCurrent] also uses the same format;
     * code is not shared because that method doesn't need to re-read
     * the laps or reformat them.
     *
     * @param writeLaps  True if the hour counter format or lap format flags have changed
     */
    internal abstract fun updateStartTimeCommentLapsView(writeLaps: Boolean)

    internal abstract fun getDefaultClockTitle(): String

    /**
     * @return  A string with the contents of the start time
     * and the user comment for the clock. If the clock has never
     * been started, this string includes only the title and the comment
     */
    internal fun getClockInfoText(): String {
        if (fmt_dow_meddate_time == null)
            fmt_dow_meddate_time = buildDateFormat(activity!!, false)

        val sb = StringBuffer()

        if (clock.wasStarted) {
            if (title.isNotEmpty())
                sb.append(title)
            else {
                sb.append(getDefaultClockTitle())
            }
            sb.append(' ')
            sb.append(resources.getText(R.string.started_at))
            sb.append(' ')
            sb.append(DateFormat.format(fmt_dow_meddate_time, clock.startTimeActual))

            if (comment.isNotEmpty()) {
                if (sb.isNotEmpty()) sb.append("\n")
                sb.append(comment)
            }
        } else {
            if (comment.isNotEmpty()) {
                if (title.isNotEmpty()) {
                    sb.append(title)
                } else {
                    sb.append(getDefaultClockTitle())
                }
                sb.append("\n")
                sb.append(comment)
            } else {
                if (title.isNotEmpty()) {
                    sb.append(title)
                }
            }
        }

        return sb.toString()
    }

    /** Run some code on UI Thread */
    internal fun post(r: Runnable) {
        try {
            activity!!.runOnUiThread(r)
        } catch (e: Throwable) {
            loge("post(); Can't post runnable to UI Thread: Is there an activity?", e)
        }
    }

    @Suppress("DEPRECATION")
    internal fun vibrateShort() {
        if (Build.VERSION.SDK_INT >= 26) {
            vib?.vibrate(VIBRATION_SHORT_EFFECT)
        } else {
            vib?.vibrate(VIBRATION_SHORT_MS)
        }
    }

    @Suppress("DEPRECATION")
    internal fun vibrateWave() {
        if (Build.VERSION.SDK_INT >= 26) {
            vib?.vibrate(FragmentClock.VIBRATION_WAVE_EFFECT)
        } else {
            vib?.vibrate(FragmentClock.VIBRATION_WAVE_MS, -1)
        }
    }

    internal inner class StartButtonListener(val currentMode: Mode) : OnClickListener {

        override fun onClick(v: View) {

            // If starting to count in countdown mode, but 'refresh' hasn't
            // been clicked, the clock shows 0:0:0 and nothing will happen.
            // Refresh automatically.
            if (currentMode == Mode.COUNTDOWN && !clock.isStarted && !clock.wasStarted) {
                (this@FragmentClock as FragmentCountdown).refreshCountdownTime(true)
            }

            // If starting to count in countdown mode, but the clock has 0:0:0,
            // nothing will happen.  Let the user know.
            if (currentMode == Mode.COUNTDOWN && !clock.isStarted &&
                    clock.hour == 0 && clock.min == 0 && clock.sec == 0 &&
                    clock.dsec == 0 && clock.csec == 0 && clock.msec == 0) {

                val resId: Int = if (clock.wasStarted)
                    R.string.countdown_completed_please_refresh
                else
                    R.string.countdown_please_set_hms
                Toast.makeText(activity, resId, Toast.LENGTH_SHORT).show()

                return  // <--- Early return: Countdown already 0:0:0:0 ---
            }

            if (clock.isStarted) logd("click(); Stop")
            else logd("click(); Start")

            clock.startOrStop()  // start or stop counting

//            saveMenuItem.isEnabled = !clock.isStarted

            vibrateShort()

            if (clock.isStarted) {
                updateStartTimeCommentLapsView(false)
            } else {
                clock.updateTimeAndGui()
            }

            // Back up current state to vm & db
            clockVM.saveClock(clock)
        }

    }

    internal inner class ResetButtonListener : OnClickListener {

        override fun onClick(v: View) {

            if (!clock.isStarted) {
                if (!clock.wasStarted) {
                    resetClockAndViews()
                } else {
                    val alert = AlertDialog.Builder(activity)
                    alert.setTitle(R.string.confirm)
                    alert.setMessage(R.string.confirm_reset_message)

                    alert.setPositiveButton(R.string.reset) { dialog: DialogInterface, whichButton: Int ->
                        resetClockAndViews()
                    }


                    alert.setNegativeButton(android.R.string.cancel) { dialog, whichButton -> }

                    alert.show()
                }
            } else {
                //Show error when currently counting
//                val toast = Toast.makeText(activity, R.string.reset_during_count, Toast.LENGTH_SHORT)
//                toast.show()

                val sb = Snackbar.make(v, R.string.reset_during_count, Snackbar.LENGTH_SHORT)
                sb.show()
            }
        }
    }

    /**
     * Add this to the Debug Log, if [DEBUG_LOG_ENABLED].
     * @since 1.6
     */
    private fun addDebugLog(msg: CharSequence) {
        if (!DEBUG_LOG_ENABLED)
            return

        if (fmt_debuglog_time == null)
            fmt_debuglog_time = buildDateFormat(activity!!, true)

        debugLog.append(DateFormat.format(fmt_debuglog_time, System.currentTimeMillis()))
        debugLog.append(": ")
        debugLog.append(msg)
        debugLog.append("\n")
    }

    companion object {

        /**
         * If true, show Debug Log in menu; see [addDebugLog].
         * The log is meant to be viewed on the device as Anstop runs, without connecting to a computer.
         * For non-development releases, be sure to set this false.
         */
        private val DEBUG_LOG_ENABLED = true

        private val ABOUT_DIALOG = 0
        private val SAVE_DIALOG = 1
        /** Dialog to set the optional [comment]  */
        private val COMMENT_DIALOG = 2
        /** Dialog to show scrolling debug log, if [DEBUG_LOG_ENABLED].  */
        private val DEBUG_LOG_DIALOG = 3

        /** Used as request code in [startActivityForResult] */
        private val SETTINGS_ACTIVITY = 4

        /** Vibrate these milliseconds when start/stop clock */
        private val VIBRATION_SHORT_MS = 50L
        private val VIBRATION_SHORT_EFFECT: VibrationEffect?

        init {
            VIBRATION_SHORT_EFFECT = if (Build.VERSION.SDK_INT >= 26)
                VibrationEffect.createOneShot(VIBRATION_SHORT_MS, VibrationEffect.DEFAULT_AMPLITUDE)
            else
                null
        }

        /**
         * Vibrate pattern for [cleanupAt0].
         * Pulse of 80 ms, wait 50, vibrate 80, etc
         */
        private val VIBRATION_WAVE_MS = longArrayOf(0, 80, 50, 80, 50, 120)
        private val VIBRATION_WAVE_EFFECT: VibrationEffect?

        init {
            VIBRATION_WAVE_EFFECT = if (Build.VERSION.SDK_INT >= 26)
                VibrationEffect.createWaveform(VIBRATION_WAVE_MS, -1)
            else
                null
        }

        /**
         * Read the boolean lap format flags from shared preferences,
         * and add them together in the format used by
         * [Clock.setLapFormat].
         * <UL>
         * <LI> <tt>lap_format_elapsed</tt> -&gt; [Clock.LAP_FMT_FLAG_ELAPSED]
        </LI> * <LI> <tT>lap_format_delta</tT> -&gt; [Clock.LAP_FMT_FLAG_DELTA]
        </LI> * <LI> <tt>lap_format_systime</tt> -&gt; [Clock.LAP_FMT_FLAG_SYSTIME]
        </LI></UL> *
         * @param settings  Shared preferences, from
         * [PreferenceManager.getDefaultSharedPreferences]
         * @return Lap format flags, or 0 if none are set in <tt>settings</tt>.
         * If this method returns 0, assume the default of
         * [Clock.LAP_FMT_FLAG_ELAPSED].
         */
        fun readLapFormatPrefFlags(settings: SharedPreferences): Int {
            val lapFmtElapsed = settings.getBoolean("lap_format_elapsed", true)
            val lapFmtDelta = settings.getBoolean("lap_format_delta", false)
            val lapFmtSystime = settings.getBoolean("lap_format_systime", false)
            var settingLap = 0
            if (lapFmtElapsed) settingLap += LapFormatter.LAP_FMT_FLAG_ELAPSED
            if (lapFmtDelta) settingLap += LapFormatter.LAP_FMT_FLAG_DELTA
            if (lapFmtSystime) settingLap += LapFormatter.LAP_FMT_FLAG_SYSTIME
            return settingLap
        }

        /**
         * Start the appropriate intent for the user to send an E-Mail or SMS
         * with these contents.
         * @param caller  The calling activity; used to launch Send intent
         * @param title Subject line for e-mail; if user has only SMS configured, this will be unused.
         * @param body  Body of e-mail
         */
        fun startSendMailIntent(caller: Activity, title: String, body: String) {
            val emailIntent = Intent(android.content.Intent.ACTION_SEND)
            val emptyRecipients = arrayOf("")
            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, emptyRecipients)
            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, title)
            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, body)
            emailIntent.type = "text/plain"
            // use Chooser, in case multiple apps are installed
            caller.startActivity(Intent.createChooser(emailIntent, caller.resources.getString(R.string.send)))
        }

        /**
         * Build [fmt_dow_meddate_time] or [fmt_debuglog_time].
         * @param ctx calling context
         * @param timeOnly  If true, wants time of day only (12- or 24-hour per user preferences).
         * If false, also wants day of week, year, month, day (formatted for locale).
         * @return a StringBuffer usable in [DateFormat.format]
         */
        fun buildDateFormat(ctx: Context, timeOnly: Boolean): StringBuffer {
            val fmt_dow_meddate = StringBuffer()

            if (!timeOnly) {
                val da = 'E'
                fmt_dow_meddate.append(da)
                fmt_dow_meddate.append(da)
                fmt_dow_meddate.append(da)
                fmt_dow_meddate.append(da)
                fmt_dow_meddate.append(' ')

                // year-month-date array will be 3 chars: yMd, Mdy, etc
                val ymd_order = DateFormat.getDateFormatOrder(ctx)
                for (c in ymd_order) {
                    fmt_dow_meddate.append(c)
                    fmt_dow_meddate.append(c)
                    if (c == 'y') {
                        fmt_dow_meddate.append(c)
                        fmt_dow_meddate.append(c)
                    } else if (c == 'M')
                        fmt_dow_meddate.append(c)

                    fmt_dow_meddate.append(' ')//don't add a space
                }
            }

            // now hh:mm:ss[ am/pm]
            val is24 = DateFormat.is24HourFormat(ctx)
            val hh = if (is24) 'k' else 'h'
            fmt_dow_meddate.append(hh)
            if (is24)
                fmt_dow_meddate.append(hh)
            fmt_dow_meddate.append(':')
            fmt_dow_meddate.append('m')
            fmt_dow_meddate.append('m')
            fmt_dow_meddate.append(':')
            fmt_dow_meddate.append('s')
            fmt_dow_meddate.append('s')
            if (!is24) {
                fmt_dow_meddate.append(' ')
                fmt_dow_meddate.append('a')
                fmt_dow_meddate.append('a')
            }

            return fmt_dow_meddate
        }
    }
}