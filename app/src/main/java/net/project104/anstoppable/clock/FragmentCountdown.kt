/*
 * Copyright (C) 2009-2011 by mj
 *   fakeacc.mj@gmail.com
 * Portions of this file Copyright (C) 2010-2012,2014-2016 Jeremy Monin
 *   jeremy@nand.net
 * Copyright (C) 2018 by civyshk
 *   civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.project104.anstoppable.clock


import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.NumberPicker
import android.widget.Toast
import net.project104.anstoppable.R


class FragmentCountdown() : FragmentClock() {

    override var mode: Mode = Mode.COUNTDOWN

    internal lateinit var startButton: Button
    internal lateinit var refreshButton: Button

    internal lateinit var secPicker: NumberPicker
    internal lateinit var minPicker: NumberPicker
    internal lateinit var hourPicker: NumberPicker

    /**
     * Fragment constructor arguments are better passed within
     * a Bundle. Use this to mimic that
     */
    @SuppressLint("ValidFragment")
    constructor (taskId: Long) : this(){
        arguments = Bundle().apply {
            putLong("TaskId", taskId)
        }
    }

    override fun setupViews() {
        super.setupViews()

        //adding spinners
        secPicker  = rootView.findViewById(R.id.secPicker)
        minPicker  = rootView.findViewById(R.id.minPicker)
        hourPicker = rootView.findViewById(R.id.hourPicker)

        secPicker.maxValue = 59
        minPicker.maxValue = 59
        hourPicker.maxValue = 24 * 365 - 1

        //set onlicklisteners
        startButton = rootView.findViewById(R.id.startButton)
        refreshButton = rootView.findViewById(R.id.refreshButton)

        startButton.setOnClickListener(StartButtonListener(Mode.COUNTDOWN))
        refreshButton.setOnClickListener(RefreshButtonListener())

        updateHourVisibility()
        updateDsecVisibility()
    }

    override fun getLayoutId(): Int
            = R.layout.fragment_countdown

    override fun currentModeAsString(): String
            = getString(R.string.countdown)

    override fun createBodyFromCurrent(): String {
        // Start time, comment, and laps are all
        // within clockInfoView's or LapView's text.
        // The same formatting is used in updateStartTimeCommentLapsView
        // and AnstopDbAdapter.getRowAndFormat. If you change this, change those to match.
        // Code is not shared because this method doesn't need to re-read
        // the laps or reformat them.

        val hoursStr: String
        if (clock.hour != 0 || clock.lapFormatter.hourFormat == HourFormat.ALWAYS_SHOW)
            hoursStr = ("\n" + hourView.text)
        else
            hoursStr = ""

        var spinnerHrs = hourPicker.value
        var spinnerMin = minPicker.value
        if (clock.lapFormatter.hourFormat == HourFormat.MINUTES_PAST_60) {
            spinnerMin += 60 * spinnerHrs
            spinnerHrs = 0
        }
        val spinnerHoursStr: String
        if (spinnerHrs != 0 || clock.lapFormatter.hourFormat == HourFormat.ALWAYS_SHOW)
            spinnerHoursStr = ("\n" + getString(R.string.hours, spinnerHrs))
        else
            spinnerHoursStr = ""

        return (getString(R.string.mode_was) + " "
                + getString(R.string.countdown)
                + hoursStr
                + "\n" + minView.text.toString() + ":" + secondsView.text.toString()
                + ":" + decimalsView.text.toString() + "\n" + getString(R.string.start_time)
                + spinnerHoursStr + "\n"
                + clock.lapFormatter.nfMS.format(spinnerMin.toLong()) + ":"
                + clock.lapFormatter.nfMS.format(secPicker.value.toLong()) + clock.lapFormatter.decimalSeparator
                + clock.lapFormatter.nfMS.format(0.toLong())
                + "\n" + clockInfoView.text.toString())
    }

    override fun getDefaultClockTitle(): String = getString(R.string.countdown)

    /**
     * For countdown mode, handle a click of the Refresh button.
     * If not started, set the clock and the displayed Hour/Min/Seconds from
     * the spinners.  Otherwise show a toast (cannot refresh during count).
     *
     * @param onlyIfZero  If true, set the clock to the input data only if the clock is currently 0:0:0:0.
     * @see [resetClockAndViews]
     */
    internal fun refreshCountdownTime(onlyIfZero: Boolean) {
        if (!clock.isStarted) {
            if (onlyIfZero && (clock.hour != 0 || clock.min != 0 || clock.sec != 0 ||
                            clock.dsec != 0 || clock.csec != 0 || clock.msec != 0)) {
                return   // <---  Early return: Not 0:0:0:0 ---
            }

            //looking for the selected Item position (is the same as the Item itself)
            //using the NumberFormat from class clock to format
            val s = secPicker.value
            val m = minPicker.value
            val h = hourPicker.value
            clock.reset(null, h, m, s)

            //Save this new state to VM & DB
            clockVM.saveClock(clock)
            //set the Views to the input data
            decimalsView.text = clock.lapFormatter.nfD.format(0)
            secondsView.text = clock.lapFormatter.nfMS.format(s.toLong())
            minView.text = clock.lapFormatter.nfMS.format(m.toLong())
            hourView.text = resources.getQuantityString(R.plurals.hours_long, h,
                    clock.lapFormatter.nfH.format(h.toLong()))

            clockInfoView.text = getClockInfoText()

            updateHourVisibility()
        } else {
            //Show error when currently counting
            val toast = Toast.makeText(activity, R.string.refresh_during_count, Toast.LENGTH_SHORT)
            toast.show()
        }
    }

    override fun resetClockAndViews() {
        if (clock.isStarted) {
            return
        }
        super.resetClockAndViews()
        clockInfoView.text = getClockInfoText()
    }

    /**
     * Format and write the start time and [comment]
     * displayed in [clockInfoView].
     * <P>
     * The same formatting is used in [AnstopDbAdapter.getRowAndFormat].
     * If you change this method, change that one to match.
     * <P>
     * [createBodyFromCurrent] also uses the same format;
     * code is not shared because that method doesn't need to re-read
     * the laps or reformat them.
     *
     * @param writeLaps  True if the hour counter format or lap format flags have changed
     */
    override fun updateStartTimeCommentLapsView(writeLaps: Boolean) {
        clockInfoView.text = getClockInfoText()
    }


    internal inner class RefreshButtonListener : View.OnClickListener {

        override fun onClick(v: View) {
            if (clock.isStarted || !clock.wasStarted) {
                refreshCountdownTime(false)
            } else {
                val alert = AlertDialog.Builder(activity)
                alert.setTitle(R.string.confirm)
                alert.setMessage(R.string.confirm_refresh_message)

                alert.setPositiveButton(R.string.refresh) {
                    dialog: DialogInterface, whichButton: Int -> refreshCountdownTime(false)
                }

                alert.setNegativeButton(android.R.string.cancel) { dialog, whichButton -> }

                alert.show()
            }
        }
    }
}
