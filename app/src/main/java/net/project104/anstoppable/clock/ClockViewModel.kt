/*
 * Copyright (c) 2018 by civyshk
 * civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.project104.anstoppable.clock

import android.app.Application
import android.util.Log
import net.project104.anstoppable.MyApp
import net.project104.anstoppable.TaskViewModel
import net.project104.anstoppable.db.TaskDao
import net.project104.anstoppable.getClassTag
import net.project104.anstoppable.handleException
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.System.currentTimeMillis

typealias DBClock = net.project104.anstoppable.db.Clock
typealias DBLap = net.project104.anstoppable.db.Lap

class ClockViewModel(app: Application)
    : TaskViewModel(app) {

    var dao: TaskDao = (app as MyApp).taskDao

    lateinit var clock: DBClock
    var laps = mutableListOf<DBLap>()

    /** Make this object load data from database. If the given taskId matches
     * the one saved by this object, it skips the unneeded initialization
     * but will still run the given block of code
     * @param newId: The id of the Clock task to load
     * @param uiTask: Task to run in uiThread when the data has been loaded
     */
    fun initId(newId: Long, uiTask : () -> Unit){
        Log.d(getClassTag(this@ClockViewModel), "Creating Clock from Id=$newId")
        if(taskId != newId) {
            taskId = newId

            doAsync({ handleException(getClassTag(this) + ".initId", it) }) {
                clock = dao.getClockByKey(taskId)
                laps = dao.getAllLaps(taskId).toMutableList()

                uiThread {
                    uiTask()
                    initialized.value = true
                }
            }
        }else{
            uiTask()
        }
    }

    /** If you don't call [initId], you must call [initNew] to create a new
     * Clock and save it in the DB
     * @param uiTask: Task to run in uiThread when the data has been created and saved
     */
    fun initNew(mode: Mode, uiTask: () -> Unit) {
        doAsync({ handleException(getClassTag(this) + ".initNew", it) }) {
            taskId = dao.insertTask(net.project104.anstoppable.db.Task())
            Log.d(getClassTag(this@ClockViewModel), "Creating new Clock, id:$taskId")
            clock = DBClock(id = taskId, mode = mode.asInt)
            dao.insertClock(clock)

//            taskId = anstopDbHelper.createNew(DEFAULT_TITLE, DEFAULT_COMMENT, mode, currentTime, -1L, 0L)
            uiThread {
                uiTask()
                initialized.value = true
            }
        }
    }

    /**
     * Update this clock state both into this viewmodel and the database
     * @param clock  Clock to save
     */
    fun saveClock(clock: Clock){
        clock.fillClockDB(this.clock)
        doAsync({ handleException(getClassTag(this) + ".saveClock", it)}) {
            dao.updateClock(this@ClockViewModel.clock)
        }
    }

    /**
     * Add a lap of a clock both to this viewmodel and the database
     * @param lap  Lap to add
     */
    fun addLap(lap: SimpleLap) {
        val newDBLap = DBLap(taskId, laps.size, lap.elapsed, lap.systime)
        laps.add(newDBLap)
        doAsync ({ handleException(getClassTag(this) + ".addLap", it)}) {
            dao.insertLap(newDBLap)
        }
    }

    /**
     * Clear all laps of a clock from this viewmodel and the database
     */
    fun clearLaps() {
        laps.clear()
        doAsync ({ handleException(getClassTag(this) + ".clearLaps", it)}) {
            dao.clearLaps(taskId)
        }
    }
}