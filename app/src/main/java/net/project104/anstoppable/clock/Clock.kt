/*
 * Copyright (C) 2009-2011 by mj
 *   fakeacc.mj@gmail.com
 * Portions of this file Copyright (C) 2010-2012,2015 Jeremy Monin
 *   jeremy@nand.net
 * Copyright (c) 2018 by civyshk
 *   civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


package net.project104.anstoppable.clock


import android.content.res.Resources
import android.os.*
import android.preference.PreferenceManager
import android.view.View

import java.text.DateFormat
import java.lang.ref.WeakReference
import java.util.*
import kotlin.math.max
import net.project104.anstoppable.*
import net.project104.anstoppable.clock.HourFormat.*

/**
 * Timer object and thread.
 *
 *
 * Has two modes ([Mode.STOP_LAP] and [Mode.COUNTDOWN]); clock's mode field is [mode].
 * Has accessible fields for the current [hour], [min], [sec], [dsec_old].
 * Has a link to the [parent] Anstop, and will sometimes read or set parent's text field contents.
 *
 *
 * Because of device power saving, there are methods to adjust the clock
 * when our app is paused/resumed: [onAppPause], [onAppResume].
 * Otherwise the counting would become inaccurate.
 *
 *
 * It has three states:
 * <UL>
 * <LI> Reset: This is the initial state.
 * In STOP mode, the hour, minute, second are 0.
 * In COUNTDOWN mode they're (h, m, s), copied from spinners when
 * the user hits the "refresh" button. </LI>
 * <LI> Started: The clock is running. </LI>
 * <LI> Stopped: The clock is not currently running, but it's changed from
 * the initial values.  That is, the clock is paused, and the user
 * can start it to continue counting. </LI>
 * </UL>
 * You can examine the current state by reading [isStarted] and [wasStarted].
 * To reset the clock again, and/or change the mode, call [reset].
 *
 *
 * When running, a thread either counts up ([threadS]) or down ([threadC]),
 * firing every 100ms.  The [parent]'s hour:minute:second.dsec displays are updated
 * through [hourh] and the rest of the handlers here.
 *
 *
 * To lap, call [addLap].  Note that persisting the lap data arrays
 * at Activity.onStop must be done in [FragmentClock], not here.
 *
 *
 * Lap display formatting is done through flags such as [LAP_FMT_FLAG_DELTA]
 * and the nested class [Clock.LapFormatter].
 */
class Clock(internal var parent: FragmentClock) {

    /**
     * Counting mode. Two possibilities:
     * <UL>
     * <LI> [Mode.STOP_LAP] counting up from 0 </LI>
     * <LI> [Mode.COUNTDOWN] counting down from a time set by the user </LI>
     * </UL>
     * @see [mode]
     * @see [changeMode]
     * @see [reset]
     */

    /**
     * Get the clock's current counting mode.
     * @return  the mode; [Mode.STOP_LAP] or [Mode.COUNTDOWN]
     * @see [changeMode]
     * @see [reset]
     */
    var mode: Mode? = null
        private set

    /** Just to show fake milliseconds */
    val random: Random = Random()

    /** Stopwatch and countdown threads sleep these milliseconds
     * between time updates. This value may change if the GUI
     * time diverge from actual clock time */
    var threadSleepTime: Long = 100

    /**
     * Hour and Lap formatting flags and fields.
     * Read-only from [FragmentClock] class.
     * The active format flags are [Clock.LapFormatter.lapFormatFlags];
     * the default is [LAP_FMT_FLAG_ELAPSED] only.
     * To change, call [setLapFormat].
     */
    var lapFormatter: LapFormatter = LapFormatter()

    /** Is the clock currently running?  */
    var isStarted = false

    /** Has the clock run since its last reset? This is not an 'isPaused' flag, because
     * it's set true when the counting begins and [isStarted] is set true.
     * <P>
     * <tt>wasStarted</tt> is set true by [startOrStop], and false by [reset].
     */
    var wasStarted = false

    private var threadS: StopWatchThread? = null
    private var threadC: CountDownThread? = null

    private val selfReference = WeakReference<Clock>(this)
    private var dsech: DSecHandler = DSecHandler(selfReference)
    private var sech: SecHandler = SecHandler(selfReference)
    private var minh: MinHandler = MinHandler(selfReference)
    private var hourh: HourHandler = HourHandler(selfReference)
    private var starth: StartButtonHandler = StartButtonHandler(selfReference)

    /** GUI Clock's milliseconds field. It's pure random
     * when the clock is running, but real when it's stopped */
    internal var msec = 0

    /** GUI Clock's centiseconds field. It's kind of random
     * when the clock is running, but real when it's stopped */
    internal var csec = 0

    /** GUI Clock's deciseconds field */
    internal var dsec = 0

    /** GUI Clock's seconds field.  */
    internal var sec = 0

    /**
     * GUI Clock's minutes field.
     * <P>
     * If [LapFormatter.hourFormat] == [HourFormat.MINUTES_PAST_60], this may be 60 or more.
     */
    internal var min = 0

    /**
     * GUI Clock's hours field.
     * <P>
     * If [LapFormatter.hourFormat] == [HourFormat.MINUTES_PAST_60], this is always 0.
     */
    internal var hour = 0

    /**
     * For lap mode, list of laps. It's empty
     * when clock isn't in lap mode
     * @see [SimpleLap]
     */
    internal var laps: MutableList<SimpleLap> = mutableListOf()

    /**
     * For countdown mode, the initial seconds, minutes, hours,
     * as set by [reset],
     * stored as a total number of seconds.
     * Used by [adjClockOnAppResume].
     */
    private var countdnTotalSeconds = 0

    /**
     * If running, get the actual start time, and the adjusted start time after pauses.
     * (If there are no pauses, they are identical.  Otherwise, the difference
     * is the amount of paused time.)
     * <P>
     * When counting up, the amount of time on the clock
     * is the current time minus <tt>clockStartTimeAdj</tt>
     * <P>
     * Taken from [System.currentTimeMillis].
     * @return  Start time, of the form used by [System.currentTimeMillis],
     *          or -1L if never started
     * @since 1.3
     */
    var startTimeActual: Long = -1L
        private set
    private var startTimeAdj: Long = -1L

    /**
     * If [wasStarted], and ! [isStarted], the
     * current time when the clock was paused by calling [startOrStop]
     * (taken from [System.currentTimeMillis]).
     * Otherwise -1.
     */
    /**
     * Get the actual stop time, if any.
     * @return  If [wasStarted], and ! [isStarted], the
     * current time when the clock was paused by calling [startOrStop]
     * (taken from [System.currentTimeMillis]).
     * Otherwise -1.
     */
    var stopTime: Long = -1L
        private set

    /**
     * Time when [android.app.Activity.onPause] was called, or <tt>-1L</tt>.
     * Used by [onAppPause], [onAppResume].
     */
    private var appPauseTime: Long = -1L

    /**
     * Time when [restoreFromDatabase] was called, or <tt>-1L</tt>.
     * Used by [onAppResume], to prevent 2 adjustments after a restore.
     */
    private var appStateRestoreTime: Long = -1L

    val currentTime: Long
        get() {
            val lastTime = if(isStarted) System.currentTimeMillis() else stopTime
            return when(mode) {
                Mode.STOP_LAP -> lastTime - startTimeAdj
                Mode.COUNTDOWN -> max(0, countdnTotalSeconds*1000L - lastTime + startTimeAdj)
                else -> throw NotImplementedError()
            }
        }

    /**
     * Record the time when [android.app.Activity.onPause] was called.
     * @see [onAppResume]
     * @since 1.3
     */
    fun onAppPause() {
        appPauseTime = System.currentTimeMillis()
        if (threadS != null && threadS!!.isAlive) {
            threadS!!.interrupt()
            threadS = null
        }
        if (threadC != null && threadC!!.isAlive) {
            threadC!!.interrupt()
            threadC = null
        }
    }

    /**
     * Adjust the clock when the app is resumed.
     * Also adjust the clock-display fields.
     * @see [onAppPause]
     * @since 1.3
     */
    fun onAppResume() {
        if (!isStarted)
            return

        if (appPauseTime > appStateRestoreTime)
            adjClockOnAppResume(true, System.currentTimeMillis())

        if (mode === Mode.STOP_LAP) {
            if (threadS != null && threadS!!.isAlive) {
                threadS!!.interrupt()
            }
            threadS = StopWatchThread()
            threadS!!.start()
        } else {
            // Mode.COUNTDOWN
            if (threadC != null && threadC!!.isAlive) {
                threadC!!.interrupt()
            }
            threadC = CountDownThread()
            threadC!!.start()
        }
    }

    /**
     * Restore our state (start time millis, etc) and keep going.
     * <P>
     * Must call AFTER the GUI elements (parent.decimalsView, etc) exist.
     * Once the GUI is set, call [changeMode] to reset clock fields
     * and then call this method.
     * <P>
     * Will call count() if <tt>anstop_state_clockStarted</tt> == 1 in the database,
     * unless we've counted down to 0:0:0.
     * For the database contents, see [net.project104.anstoppable.db.Clock].
     * <P>
     *
     * @param dbClock  clock entry from database
     * @return true if clock was running when saved, false otherwise
     */
    fun restoreFromDatabase(dbClock: DBClock, dbLaps: List<DBLap>, res: Resources) : Boolean{

        var restoredAtTime = System.currentTimeMillis()
        appStateRestoreTime = restoredAtTime

        val modeInt = dbClock.mode
        if(mode?.asInt != modeInt)
            logw("restoreFromDatabase(); new mode from database doesn't match the previously set mode")

        // set mode to ensure consistent state; should be set already
        // by changeMode before this method was called.
        mode = Mode[modeInt]
        if (mode != parent.mode)
            loge("restoreFromDatabase(); mode from database and parent fragment don't match")

        startTimeActual = dbClock.startTimeActual
        startTimeAdj = dbClock.startTimeAdj
        stopTime = dbClock.stopTime
        countdnTotalSeconds = dbClock.countdown
        wasStarted = dbClock.wasStarted
        val mustStart = dbClock.isStarted

        lapFormatter.restoreFromDatabase(dbClock)

        //Because lapFormatter.numberDecimals has just been set,
        //update fake view width:
        parent.updateFakeClockWidth()

        val currentTime = when (mode) {
            Mode.COUNTDOWN -> countdnTotalSeconds * 1000L - if(mustStart)
                restoredAtTime - startTimeAdj
            else
                stopTime - startTimeAdj

            Mode.STOP_LAP -> if(mustStart) restoredAtTime - startTimeAdj else stopTime - startTimeAdj
            else -> throw RuntimeException("Dev, implement missing clock mode")
        }

        val timeFields = splitTime(currentTime)
        hour = timeFields.hour
        min = timeFields.min
        sec = timeFields.sec
        dsec = timeFields.dsec
        csec = timeFields.csec
        msec = timeFields.msec

        if (parent.mode == Mode.STOP_LAP) {
            laps = dbLaps.map{
                SimpleLap(it.systime, it.elapsed)
            }.toMutableList()

            //TODO could this be done by fragment?
            //Lap formatter should have access to fragment resources and clock fields
            parent.lapsContent.setLength(0)
            for(i in laps.indices) {
                parent.lapsContent.append('\n')
                lapFormatter.formatTimeLap(parent.lapsContent, true,
                        -1, -1, -1, -1, -1, -1,
                        i, laps[i].elapsed, laps[i].systime, laps, res)
            }
        }

        parent.title = dbClock.title
        parent.comment = dbClock.comment

        parent.updateStartTimeCommentLapsView(true)

        if (parent.mode == Mode.COUNTDOWN) {
            val (h, m, s, _) =
                    splitTime(countdnTotalSeconds*1000L, skipFormat=true)
            (parent as FragmentCountdown).hourPicker.value = h
            (parent as FragmentCountdown).minPicker.value = m
            (parent as FragmentCountdown).secPicker.value = s
        }

        if (threadS != null && threadS!!.isAlive)
            threadS!!.interrupt()
        if (threadC != null && threadC!!.isAlive)
            threadC!!.interrupt()

        // Adjust and continue the clock thread:
        // re-read current time for most accuracy
        if (mustStart) {
            restoredAtTime = System.currentTimeMillis()
            appStateRestoreTime = restoredAtTime
            adjClockOnAppResume(true, restoredAtTime)
        } else {
            adjClockOnAppResume(false, 0L)
        }

        isStarted = false  // must be false before calling startOrStop()
        if (mustStart) {
            // Read the values from text elements we've just set, and start it:
            // In countdown mode, will check if we're past 0:0:0 by now.
            startOrStop(updateClockTimes=false)
        }
        return isStarted
    }

    /**
     * Save the clock's current state to a [DBClock]
     * @param dbClock  Object into which save data
     */
    fun fillClockDB(dbClock: DBClock){
        dbClock.mode = mode!!.asInt
        dbClock.startTimeActual = startTimeActual
        dbClock.startTimeAdj = startTimeAdj
        dbClock.stopTime = stopTime
        dbClock.countdown = countdnTotalSeconds
        dbClock.wasStarted = wasStarted
        dbClock.isStarted = isStarted
        dbClock.comment = parent.comment
        dbClock.title = parent.title

        lapFormatter.fillClockDB(dbClock)
    }

    /**
     * Adjust the clock fields ([hour], [min], etc)
     * and the display fields ([FragmentClock.hourView], etc)
     * based on the application being paused for a period of time.
     * <P>
     * If <tt>updateGuiFields</tt> is true, do not call unless [isStarted].
     * <P>
     * Be sure these fields are restored from save state before calling:
     * [startTimeAdj], [countdnTotalSeconds],
     * [lapFormatter.hourFormat][LapFormatter.hourFormat]
     * <P>
     * Used with [onAppResume] and [restoreFromDatabase].
     *
     * @param updateGuiFields  If false, update the display fields based on
     * the current hour, min, sec, dsec internal field values,
     * and skip adjusting those internal values.
     * Then <tt>resumedAtTime</tt> is ignored.
     * @param resumedAtTime  Time when the app was resumed, from [System.currentTimeMillis]
     */
    private fun adjClockOnAppResume(updateGuiFields: Boolean, resumedAtTime: Long) {
        if (updateGuiFields) {

            // based on our mode, adjust dsec, sec, min, hour:
            val timeFields = when (mode) {
                Mode.STOP_LAP -> splitTime(resumedAtTime - startTimeAdj)
                Mode.COUNTDOWN -> splitTime(countdnTotalSeconds * 1000L - (resumedAtTime - startTimeAdj))
                else -> throw NotImplementedError()
            }

            msec = timeFields.msec
            csec = timeFields.csec
            dsec = timeFields.dsec
            sec = timeFields.sec
            min = timeFields.min
            hour = timeFields.hour
        }

        dsech.sendEmptyMessage(0)
        sech.sendEmptyMessage(0)
        minh.sendEmptyMessage(0)
        hourh.sendEmptyMessage(0)
    }

    /**
     * Split some amount of milliseconds into appropriate values of
     * hour, minute, second & decisecond. If [LapFormatter.hourFormat] is set to hide
     * hours, all hours will be summed as minutes
     * @param time Time in milliseconds
     * @param skipFormat If true, the method will always fill the hour field with
     *          its appropriate value independently of the setting in [LapFormatter.hourFormat]
     * @return A [SplitTime] object containing hour, minute, second & decisecond
     */
    private fun splitTime(time: Long, skipFormat: Boolean = false) : SplitTime {
        val result = SplitTime()
        var ttotal = if(time > 0) time else 0L

        result.msec = (ttotal % 10).toInt()
        ttotal /= 10
        result.csec = (ttotal % 10).toInt()
        ttotal /= 10
        result.dsec = (ttotal % 10).toInt()
        ttotal /= 10
        result.sec = (ttotal % 60).toInt()
        ttotal /= 60
        if (!skipFormat && lapFormatter.hourFormat != MINUTES_PAST_60) {
            result.min = (ttotal % 60).toInt()
            ttotal /= 60
            result.hour = ttotal.toInt()
        } else {
            result.min = ttotal.toInt()
            result.hour = 0
        }
        return result
    }

    /**
     * Format some value in milliseconds, using [LapFormatter], and
     * append to a StringBuilder
     * @param sb  StringBuilder, non null
     * current value in the format "#h mm:ss:d" will be appended to sb
     * depending on [LapFormatter.lapFormatFlags].
     * if [LapFormatter.lapFormatFlags] includes [LapFormatter.LAP_FMT_FLAG_DELTA],
     * and [laps].size &ge; 1, then [laps]\[currentLap - 1\]
     * must be accurate to calculate the delta.
     * @param elapsedMillis  Time to format and append
     * @param withLap  If true, sb will have the lap number too;
     * sb's appended format will be "lap. #h mm:ss:d"
     */
    fun appendLap(sb: StringBuilder, elapsedMillis: Long, withLap: Boolean, res: Resources) {

        val timeFields = splitTime(elapsedMillis)

        val ms = timeFields.msec
        val cs = timeFields.csec
        val ds = timeFields.dsec
        val s = timeFields.sec
        val m = timeFields.min
        val h = timeFields.hour

        lapFormatter.formatTimeLap(sb, withLap, h, m, s, ds, cs, ms,
                laps.size, elapsedMillis, System.currentTimeMillis(),
                laps, res)
    }

    /**
     * Set the clock's [LapFormatter.hourFormat], adjusting [hour] and
     * [min] if necessary (minutes past 60 and hours to 0, or reversing that).
     * @param newFormat  An hour format: [HourFormat.HIDE_IF_0],
     * [HourFormat.ALWAYS_SHOW], or [HourFormat.MINUTES_PAST_60]
     * @see [setLapFormat]
     * @since 1.6
     */
    fun setHourFormat(newFormat: HourFormat) {
        val wasMinPast60 = lapFormatter.hourFormat == MINUTES_PAST_60
        val newMinPast60 = newFormat == MINUTES_PAST_60
        lapFormatter.hourFormat = newFormat

        if (wasMinPast60 != newMinPast60) {
            var wantsRefresh = false

            if (newMinPast60) {
                if (hour > 0) {
                    min += 60 * hour
                    hour = 0
                    wantsRefresh = true
                }
            } else {
                if (min >= 60) {
                    hour = min / 60
                    min %= 60
                    wantsRefresh = true
                }
            }

            if (wantsRefresh) {
                hourh.sendEmptyMessage(0)
                minh.sendEmptyMessage(0)
            }
        }
    }

    /**
     * Update this clock's members: [hour], [min], [sec], [dsec],
     * [csec], [msec]
     * Also update Gui views if needed
     *
     * @param force  if true, update and print [dsec], [csec], [msec] in GUI
     * even if they already have right values. Useful when changing
     * [LapFormatter.numberDecimals] and those decimals need to be
     * redrawn
     */
    fun updateTimeAndGui(force: Boolean = false) {
        val time: Long = currentTime
        val timeFields = splitTime(time)
        if (force || timeFields.dsec != dsec || timeFields.csec != csec || timeFields.msec != msec) {
            dsec = timeFields.dsec
            csec = timeFields.csec
            msec = timeFields.msec
            dsech.sendEmptyMessage(0)
        }
        if (timeFields.sec != sec) {
            sec = timeFields.sec
            sech.sendEmptyMessage(0)
        }
        if (timeFields.min != min) {
            min = timeFields.min
            minh.sendEmptyMessage(0)
        }
        if (timeFields.hour != hour) {
            hour = timeFields.hour
            hourh.sendEmptyMessage(0)
        }
    }

    /**
     * Set the lap format flags.
     * @param newFormatFlags  Collection of flags, such as [LAP_FMT_FLAG_DELTA]; not 0
     * @param formatForSysTime Short time format in case [LAP_FMT_FLAG_SYSTIME] is used; not null.
     * Value should be <tt>android.text.format.DateFormat.getTimeFormat(getApplicationContext())</tt>.
     * Note that <tt>getTimeFormat</tt> gives hours and minutes, it has no standard way to include the seconds;
     * if more precision is needed, the user can get it from elapsed or delta seconds.
     * @throws IllegalArgumentException if flags &lt;= 0, or <tt>formatForSysTime == null</tt>
     * @see [setHourFormat]
     */
    @Throws(IllegalArgumentException::class)
    fun setLapFormat(newFormatFlags: Int, formatForSysTime: DateFormat) {
        lapFormatter.setLapFormat(newFormatFlags, formatForSysTime)
    }

    /**
     * Take a lap now.  Optionally append the current lap time to a buffer.
     * <P>
     * If not [wasStarted], sets that flag so the new lap time-of-day info
     * won't be lost without UI confirmation if Reset button is pressed.
     * If also not [isStarted] and the hh:mm:ss:d fields are all 0,
     * sets the start time and stop time to the lap's time of day.
     *
     * @param sb  A buffer to which the lap info
     * will be appended, in the format "lap. #h mm:ss:d"
     * depending on [LapFormatter.lapFormatFlags].
     * @since 1.5
     */
    fun addLap(sb: StringBuilder, res: Resources) {
        val lapNow = System.currentTimeMillis()

        val lapElapsed = currentTime
        appendLap(sb, lapElapsed,true, res)
        laps.add(SimpleLap(lapNow, lapElapsed))

        if (!wasStarted) {
            wasStarted = true

            if (!isStarted && hour == 0 && min == 0 && sec == 0 && dsec == 0) {

                startTimeActual = lapNow
                startTimeAdj = lapNow
                stopTime = lapNow
            }
        }

    }

    /**
     * Reset the clock while stopped, and maybe change modes.  [isStarted] must be false.
     * If <tt>newMode</tt> is [Mode.STOP_LAP], the clock will be reset to 0,
     * and <tt>h</tt>, <tt>m</tt>, <tt>s</tt> are ignored.
     *
     * @param newMode  new mode to set, or null to leave as is
     * @param h  for countdown mode, hour to reset the clock to
     * @param m  minute to reset the clock to
     * @param s  second to reset the clock to
     * @return true if was reset, false if was not reset because [isStarted] is true.
     * @see [changeMode]
     */
    fun reset(newMode: Mode?, h: Int, m: Int, s: Int): Boolean {
        var m = m
        if (isStarted)
            return false

        if (newMode != null)
            mode = newMode

        wasStarted = false
        appPauseTime = -1L
        appStateRestoreTime = -1L
        stopTime = -1L
        startTimeActual = -1L
        startTimeAdj = -1L

        laps.clear()
        if (mode === Mode.STOP_LAP) {
            hour = 0
            min = 0
            sec = 0
        } else {  // COUNTDOWN
            if (lapFormatter.hourFormat != MINUTES_PAST_60) {
                hour = h
            } else {
                m += 60 * h
                hour = 0
            }
            min = m
            sec = s
            countdnTotalSeconds = (h * 60 + m) * 60 + s
        }
        dsec = 0
        csec = 0
        msec = 0

        return true
    }

    /**
     * Change the current mode.
     * <P>
     * If the current mode is already <tt>newMode</tt>, change it anyway;
     * calls <tt>reset</tt> to update all fields.
     * @see [reset]
     * @param newMode  The new mode; [Mode.STOP_LAP] or [Mode.COUNTDOWN]
     */
    fun changeMode(newMode: Mode) {
        reset(newMode, 0, 0, 0)
    }

    /**
     * Start or stop(pause) counting.
     * <P>
     * For <tt>COUNTDOWN</tt> mode, you must first call [reset]
     * or set the [hour], [min], [sec] fields.
     * <P>
     * If <tt>wasStarted</tt>, and if <tt>stopTime</tt> != -1,
     * will update <tt>startTimeAdj</tt>
     * (based on current time and <tt>stopTime</tt>)
     * before starting the counting thread.
     * This assumes that the counting was recently paused by the
     * user, and is starting up again.
     *
     * @param updateClockTimes  If true (default), it will update
     * clock fields [startTimeActual], [startTimeAdj] and [stopTime]
     * as normal. Use false when they are already set and should
     * remain unchanged, usually when starting a clock which recently
     * loaded its data from database or other sources
     */
    fun startOrStop(updateClockTimes: Boolean = true) {
        val now = System.currentTimeMillis()

        if (!isStarted) { // START

            if(updateClockTimes) {
                if (!wasStarted) { // Start for the first time
                    startTimeActual = now
                    startTimeAdj = startTimeActual
                } else {
                    startTimeAdj += now - stopTime
                }
            }

            isStarted = true
            wasStarted = true
            when (mode) {
                Mode.STOP_LAP -> {
                    if (threadS != null && threadS!!.isAlive) {
                        threadS!!.interrupt()
                    }
                    threadS = StopWatchThread()
                    threadS!!.start()
                }
                Mode.COUNTDOWN -> {
                    if (threadC != null && threadC!!.isAlive) {
                        threadC!!.interrupt()
                    }
                    if (msec > 0 || csec > 0 || dsec > 0 || sec > 0 || min > 0 || hour > 0) {
                        threadC = CountDownThread()
                        threadC!!.start()
                    } else {
                        isStarted = false
                    }
                }
            }

        } else { // STOP
            isStarted = false
            stopTime = now

            when(mode) {
                Mode.STOP_LAP -> if (threadS!!.isAlive) threadS!!.interrupt()
                Mode.COUNTDOWN -> if (threadC!!.isAlive) threadC!!.interrupt()
            }
        }

        //Update GUI button
        starth.sendEmptyMessage(0)
    }

    /**
     * Because Thread.sleep(100) can't sleep exactly 100 ms,
     * call this method every frame to gracefully adjust the
     * sleep time.
     * If not, GUI clock will probably diverge from actual clock
     */
    private fun adjustSleepTime(){
        val approximateTime = SplitTime(hour, min, sec, dsec).asMilliseconds()
        threadSleepTime = max(0, 100 + (approximateTime - currentTime) / if(mode == Mode.STOP_LAP) 10 else -10)
    }

    /**
     * Increment GUI clock fields ([hour], [min], etc),
     * one decisecond and update GUI views when needed.
     *
     *
     * Note that [msec] and [csec] aren't really calculated,
     * so you must get [currentTime] to get the actual values of
     * centiseconds and milliseconds
     */
    private fun incrementOneDecisecond(){

        /* Thread sleeps around 100ms, so the GUI msec and csec
         are simulated as long as their actual values are not needed */
        msec = random.nextInt(10)
        csec = (csec + 3) % 10 //Seems to go 0 -> 3 -> 6 -> 9 -> 2, etc
        dsec++

        if (dsec == 10) {
            sec++
            dsec = 0
            sech.sendEmptyMessage(0)

            if (sec == 60) {
                min++
                sec = 0
                minh.sendEmptyMessage(0)

                if (min >= 60 && lapFormatter.hourFormat != HourFormat.MINUTES_PAST_60) {
                    hour++
                    min -= 60
                    hourh.sendEmptyMessage(0)
                    minh.sendEmptyMessage(0)
                }
            }
        }

        dsech.sendEmptyMessage(0)
    }

    /**
     * Decrement GUI clock fields ([hour], [min], etc),
     * one decisecond and update GUI views when needed.
     *
     *
     * Note that [msec] and [csec] aren't really calculated,
     * so you must get [currentTime] to get the actual values of
     * centiseconds and milliseconds
     */
    fun decrementOneDecisecond() {
        if (dsec == 0) {
            if (sec == 0) {
                if (min == 0) {
                    if (hour != 0) {
                        hour--
                        min = 60
                        hourh.sendEmptyMessage(0)
                        minh.sendEmptyMessage(0)
                    }
                }

                if (min != 0) {
                    min--
                    sec = 60
                    minh.sendEmptyMessage(0)
                }
            }

            if (sec != 0) {
                sec--
                dsec = 10
                sech.sendEmptyMessage(0)
            }
        }
        dsec--

        /* Thread sleeps around 100ms, so the GUI msec and csec
           are simulated until their actual values are needed */
//        csec = random.nextInt(10)
        csec = (csec + 7) % 10 //Seems to go 9 -> 6 -> 3 -> 0 -> 7, etc
        msec = random.nextInt(10)

        dsech.sendEmptyMessage(0)
    }

    /**
     * Lap mode, count up from 0.
     * @see CountDownThread
     */
    private inner class StopWatchThread : Thread() {
        init {
            isDaemon = true
        }

        override fun run() {

            while(true) {
                incrementOneDecisecond()
                adjustSleepTime()

                try {
                    Thread.sleep(threadSleepTime)
                } catch (e: InterruptedException) {
                    return
                }
            }
        }
    }

    /**
     * Countdown mode, count down to 0.
     * Before starting this thread, set non-zero h:m:s.d by calling [Clock.reset].
     * Otherwise the thread immediately stops at 00:00:00.0.
     * @see StopWatchThread
     */
    private inner class CountDownThread : Thread() {

        init {
            isDaemon = true
        }

            /**
         * Call from run() when countdown reaches 00h 00:00.0XX
         * Clears [Clock.isStarted] and re-enables Save menu item.
         * Vibrate if `vibrate_countdown_0` preference is set.
         * Thread should return after calling this method, it's reached the end of the countdown.
         */
        private fun cleanupAt0() {

            isStarted = false

            parent.post(Runnable {
                //TODO fix and enable
//                parent.saveMenuItem.isEnabled = true
            })

            // Make stopTime exactly match the end of countdown, needed because
            // mainThread could call clock.updateTimeAndGui() in mainThread
            stopTime = countdnTotalSeconds*1000L + startTimeAdj

            // This fields are still != 0, so fix them
            msec = 0
            csec = 0
            dsech.sendEmptyMessage(0)

            val settings = PreferenceManager.getDefaultSharedPreferences(parent.context)

            if (settings.getBoolean(PREF_VIBRATE_COUNTDOWN_0, true)) {
                parent.vibrateWave()
            }

            // After stopping, back up current state to database
            // as when Start/Stop button is pressed
            parent.clockVM.saveClock(this@Clock)
        }

        override fun run() {

            while(true) {
                if (hour == 0 && min == 0 && sec == 0 && dsec == 0) {
                    cleanupAt0()
                    return
                }

                decrementOneDecisecond()
                adjustSleepTime()

                try {
                    Thread.sleep(threadSleepTime)
                } catch (e: InterruptedException) {
                    return
                }

            }
        }
    }

    companion object {


        /**
         * Convert these three decimals into one number appropriate for
         * the decimal TextView.
         *
         * @return 0 if [LapFormatter.numberDecimals] == 0
         * ds if [LapFormatter.numberDecimals] == 1;
         * ds*10 + cs if [LapFormatter.numberDecimals] == 2;
         * ds*100 + cs*10 + ms elsewhere
         */
        fun mergeDecimals(ds: Int, cs: Int, ms: Int, numberDecimals: Int) : Int =
                when(numberDecimals) {
                    0 -> 0
                    1 -> ds
                    2 -> ds * 10 + cs
                    else -> (ds * 10 + cs) * 10 + ms
                }
    }
}



private class DSecHandler(val clock: WeakReference<Clock>) : Handler() {
    override fun handleMessage(msg: Message) {
        val c = clock.get() ?: return
        val guiDecimals = Clock.mergeDecimals(c.dsec, c.csec, c.msec, c.lapFormatter.numberDecimals)
        c.parent.decimalsView.text = c.lapFormatter.nfD.format(guiDecimals)
    }
}

private class SecHandler(val clock: WeakReference<Clock>) : Handler() {
    override fun handleMessage(msg: Message) {
        val c = clock.get() ?: return
        c.parent.secondsView.text = c.lapFormatter.nfMS.format(c.sec.toLong())
    }
}

private class MinHandler(val clock: WeakReference<Clock>) : Handler() {
    override fun handleMessage(msg: Message) {
        val c = clock.get() ?: return
        c.parent.minView.text = c.lapFormatter.nfMS.format(c.min.toLong())
    }
}

private class HourHandler(val clock: WeakReference<Clock>) : Handler() {
    override fun handleMessage(msg: Message) {
        val c = clock.get() ?: return
        c.parent.hourView.text = c.parent.resources.getQuantityString(R.plurals.hours_long, c.hour,
                c.lapFormatter.nfH.format(c.hour.toLong()))

        // Is hour visibility wanted?
        val hourIsVis = c.parent.hourView.visibility == View.VISIBLE
        val hourWantVis = c.hour != 0 || c.lapFormatter.hourFormat == ALWAYS_SHOW
        if (hourIsVis != hourWantVis) {
            val hourVis = if (hourWantVis) View.VISIBLE else View.INVISIBLE

            c.parent.hourView.visibility = hourVis
        }
    }
}

private class StartButtonHandler(val clock: WeakReference<Clock>) : Handler() {
    override fun handleMessage(msg: Message?) {
        val c = clock.get() ?: return
        c.parent.btnStartStop.text = if (c.isStarted) {
            c.parent.getString(R.string.stop)
        }else{
            c.parent.getString(R.string.start)
        }
    }
}


data class SplitTime(var hour: Int,
                     var min: Int,
                     var sec: Int,
                     var dsec: Int,
                     var csec: Int=0,
                     var msec: Int=0) {

    /**
     * Construct a [SplitTime] set to zero
     */
    constructor() : this(0, 0, 0, 0, 0, 0)

    /**
     * Use this constructor when the initialization time
     * is given in deciseconds
     */
    constructor(dsec: Long) : this(
            (dsec/3600000).toInt(),
            (dsec/60000).toInt(),
            (dsec/1000).toInt(),
            dsec.toInt(),
            0,
            0)

    fun asMilliseconds() : Long =
            (((((hour*60L + min)*60 + sec)*10 + dsec)*10 + csec)*10 + msec)
}