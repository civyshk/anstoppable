/*
 * Copyright (c) 2018 by civyshk
 * civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.project104.anstoppable.clock


import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import net.project104.anstoppable.R
import android.content.Intent
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.widget.EditText

class ClockDialog() : DialogFragment() {

    lateinit var etTitle: EditText
    lateinit var etComment: EditText

    /**
     * Fragment constructor arguments are better passed within
     * a Bundle. Use this to mimic that
     */
    @SuppressLint("ValidFragment")
    constructor (title: String, comment: String) : this(){
        arguments = Bundle().apply {
            putString(DATA_CLOCK_TITLE, title)
            putString(DATA_CLOCK_COMMENT, comment)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        // Use activity's layoutInflater instead of dialogFragment's
        // or die in an infinite recursion stack overflow
        val content = activity!!.layoutInflater.inflate(R.layout.dialog_clock, null)

        etTitle = content.findViewById<EditText>(R.id.etClockTitle)
        etTitle.setText(arguments?.getString(DATA_CLOCK_TITLE) ?: "")

        etComment = content.findViewById<EditText>(R.id.etClockComment)
        etComment.setText(arguments?.getString(DATA_CLOCK_COMMENT) ?: "")

        // Use the Builder class for convenient dialog construction
        val builder = AlertDialog.Builder(activity!!)
        builder
                .setMessage(R.string.clock_dialog_title)
                .setView(content)
                .setPositiveButton(android.R.string.ok) { dialog, id ->

                    val title = etTitle.text.toString().trim { it <= ' ' }
                    val comment = etComment.text.toString().trim { it <= ' ' }

                    val data = Intent()
                    data.putExtra(DATA_CLOCK_TITLE, title)
                    data.putExtra(DATA_CLOCK_COMMENT, comment)

                    targetFragment?.onActivityResult(targetRequestCode, Activity.RESULT_OK, data);
//                    dismiss()//try delete this
                }
                .setNegativeButton(R.string.cancel) { _, _ -> }

        // Create the AlertDialog object and return it
        return builder.create()
    }

    companion object {
        val DATA_CLOCK_TITLE = "dataClockTitle"
        val DATA_CLOCK_COMMENT = "dataClockComment"
    }
}
