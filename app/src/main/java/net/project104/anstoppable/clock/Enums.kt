/*
 * Copyright (c) 2018 by civyshk
 * civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.project104.anstoppable.clock

enum class Mode(val asInt: Int) {

    /**
     * Count time from zero upwards, allowing to
     * save partial times (laps)
     */
    STOP_LAP(0),

    /**
     * Count down from a user defined time. There is
     * no support for laps in this mode
     */
    COUNTDOWN(1);

    companion object {
        operator fun get(v: Int): Mode = when (v) {
            0 -> STOP_LAP
            1 -> COUNTDOWN
            else -> STOP_LAP
        }
    }
}

enum class HourFormat(val asInt: Int) {

    /**
     * Hour field format: Hide hours if 0 (default asInt).
     * Current setting is in [Clock.LapFormatter.hourFormat].
     * @since 1.6
     */
    HIDE_IF_0(0),

    /**
     * Hour field format: Always show hours, even if 0.
     * Current setting is in [Clock.LapFormatter.hourFormat].
     * @since 1.6
     */
    ALWAYS_SHOW(1),

    /**
     * Hour field format: No hours field, [Clock.min] keep increasing past 60.
     * Current setting is in [Clock.LapFormatter.hourFormat].
     * When this format is active, [Clock.hour] is always 0.
     * @since 1.6
     */
    MINUTES_PAST_60(2);

    companion object {
        operator fun get(v: Int): HourFormat = when (v) {
            0 -> HIDE_IF_0
            1 -> ALWAYS_SHOW
            2 -> MINUTES_PAST_60
            else -> HIDE_IF_0
        }
    }
}