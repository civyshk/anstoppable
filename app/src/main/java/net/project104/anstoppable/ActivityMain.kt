/*
 * Copyright (c) 2018 by civyshk
 * civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.project104.anstoppable

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.util.Linkify
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import android.widget.TextView

import kotlinx.android.synthetic.main.activity_main.*
import net.project104.anstoppable.clock.FragmentCountdown
import net.project104.anstoppable.clock.FragmentStopwatch
import net.project104.anstoppable.counter.FragmentCounter
import net.project104.anstoppable.master.FragmentMaster
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.longToast
import org.jetbrains.anko.uiThread
import kotlin.math.roundToInt

class ActivityMain : ThemedActivity(
        R.style.AnstopThemeLight_NoActionBar,
        R.style.AnstopThemeDark_NoActionBar
), FragmentMaster.Listener {

    var taskId: Long = -1L
    private var taskType: TaskType? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        logd("onCreate()")
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        //If null, the activity hasn't loaded a fragment
        if(savedInstanceState == null) {
            logd("savedInstanceState=null")
            setFragmentMaster()
            val preferences = getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE)
            taskId = preferences.getLong(PREF_LAST_TASK, -1L)
            if(taskId == -1L){
                logd("There is no previous task")
            }else {
                logd("Previous task is: $taskId")
                doAsync({ handleException(getClassTag(this) + ".onCreate", it) }) {
                    val dbHelper = (application as MyApp).dbHelper
                    if (dbHelper.isCounter(taskId)) {
                        taskType = TaskType.COUNTER
                    } else if(dbHelper.isClock(taskId)) {
                        taskType = if (dbHelper.isStopwatch(taskId)) {
                            TaskType.STOPWATCH
                        }else{
                            TaskType.COUNTDOWN
                        }
                    }

                    uiThread {
                        onOpenTask (taskId, taskType)
                    }
                }
            }
        }

        fab.setOnClickListener { view ->
            openMenuNewTask(fab)
        }

//        val density = DisplayMetrics.DENSITY_DEFAULT * (resources?.displayMetrics?.density ?: 1.0f)
//        supportActionBar?.title =
//                when {
//                    density >= DisplayMetrics.DENSITY_XXXHIGH -> "XXXH"
//                    density >= DisplayMetrics.DENSITY_XXHIGH -> "XXH"
//                    density >= DisplayMetrics.DENSITY_XHIGH -> "XH"
//                    density >= DisplayMetrics.DENSITY_HIGH -> "H"
//                    density >= DisplayMetrics.DENSITY_MEDIUM -> "M"
//                    density >= DisplayMetrics.DENSITY_LOW -> "L"
//                    else -> "?"
//                } +
//                "(${density.toInt()}) " +
//                ((resources?.displayMetrics?.widthPixels ?: 0) / density * DisplayMetrics.DENSITY_DEFAULT) +
//                "x" +
//                ((resources?.displayMetrics?.heightPixels ?: 0) / density * DisplayMetrics.DENSITY_DEFAULT)
    }

    public override fun onResume() {
        logd("onResume()")
        super.onResume()
    }

    private fun openMenuNewTask(anchor: View) {
        val popMenu = PopupMenu(this, anchor)
        popMenu.inflate(R.menu.menu_new_task)
        popMenu.setOnMenuItemClickListener ( ::onNewTaskMenuItemClicked )
        popMenu.show()
    }

    private fun onNewTaskMenuItemClicked(item: MenuItem) : Boolean {
        when (item.itemId) {
            R.id.action_new_counter -> setFragmentCounter()
            R.id.action_new_stopwatch -> setFragmentStopwatch()
            R.id.action_new_countdown -> setFragmentCountdown()
            else -> longToast("Stopwatch not implemented")
        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        hackMenuIcons(menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.menu_about -> showDialogAbout()
            R.id.menu_settings -> {
                val fragment = supportFragmentManager.findFragmentById(R.id.fragmentRoot)
                val tryFragmentOpenSettings = fragment.onOptionsItemSelected(item)
                if(!tryFragmentOpenSettings){
                    val i = Intent()
                    i.setClass(this, SettingsActivity::class.java)
                    i.putExtra("TaskType", -1)
                    startActivity(i)
                }
                return true
            }
            in listOf(R.id.action_new_counter, R.id.action_new_stopwatch, R.id.action_new_countdown) ->
                return onNewTaskMenuItemClicked(item)

        }

        return super.onOptionsItemSelected(item)
    }

    private fun setFragmentTask(fragment: Fragment){

        //Allow only one fragmentTask in backStack
        val alreadyInTask = supportFragmentManager.findFragmentByTag("FragmentTask") != null
        if (alreadyInTask) {
            supportFragmentManager.popBackStack()
        }

        supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentRoot, fragment, "FragmentTask")
                .addToBackStack("FragmentTask")
                .commit()
    }

    private fun setFragmentCounter(taskId: Long = -1){
        setFragmentTask(FragmentCounter(taskId))
    }

    private fun setFragmentStopwatch(taskId: Long = -1){
        setFragmentTask(FragmentStopwatch(taskId))
    }

    private fun setFragmentCountdown(taskId: Long = -1){
        setFragmentTask(FragmentCountdown(taskId))
    }

    private fun setFragmentMaster(){

        //TODO fragmentMaster, and maybe others, need some kind of loading image

        supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentRoot, FragmentMaster(), "FragmentMaster")
                .commit()
    }

    override fun onOpenTask(taskId: Long, type: TaskType?) {
        logd("Open task $taskId (${type?.name})")
        when (type) {
            TaskType.COUNTER -> setFragmentCounter(taskId)
            TaskType.STOPWATCH -> setFragmentStopwatch(taskId)
            TaskType.COUNTDOWN -> setFragmentCountdown(taskId)
            null -> logw("Can't open undefined task of null type")
        }
    }

    override fun onDeleteTask(taskId: Long) {
        logd("Nothing to do")
    }

    private fun showDialogAbout() {
        // dialog text includes clickable URLs

        val tvAbout = TextView(this).apply {
            val p = resources.getDimension(R.dimen.default_margin).roundToInt()
            setPadding(p, p, p, p)

            text = SpannableStringBuilder(getString(R.string.about_dialog, MyApp.APP_VERSION)).also {
                Linkify.addLinks(it, Linkify.WEB_URLS)
            }

            movementMethod = LinkMovementMethod.getInstance()
        }

        AlertDialog.Builder(this).setView(tvAbout)
                .setCancelable(true)
                .setPositiveButton(android.R.string.ok) { dialog, id -> dialog.dismiss() }
                .create()
                .show()
    }
}
