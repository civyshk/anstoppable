/*
 * Copyright (C) 2009-2010 by mj
 *   fakeacc.mj@gmail.com
 * Portions of this file Copyright (C) 2012 Jeremy Monin
 *   jeremy@nand.net
 * Copyright (C) 2018 by civyshk
 *   civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.project104.anstoppable

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.preference.CheckBoxPreference
import android.support.v7.preference.PreferenceFragmentCompat
import android.widget.Toast

class SettingsFragment()
    : PreferenceFragmentCompat(),
        SharedPreferences.OnSharedPreferenceChangeListener {

    @SuppressLint("ValidFragment")
    constructor(taskType: TaskType?): this(){
        arguments = Bundle().apply {
            putInt("TaskType", taskType?.asInt ?: -1)
        }
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {

        //			getPreferenceManager().setSharedPreferencesName(Settings.PREFERENCES_NAME);
        //			getPreferenceManager().setSharedPreferencesMode(Context.MODE_WORLD_READABLE);

        val taskTypeInt = arguments?.getInt("TaskType") ?: -1
        val taskType = TaskType[taskTypeInt]

        // add the main xml
        addPreferencesFromResource(R.xml.settings_main)

        when(taskType){
            TaskType.STOPWATCH -> {
                addPreferencesFromResource(R.xml.settings_clock)
                addPreferencesFromResource(R.xml.settings_stopwatch)
            }
            TaskType.COUNTDOWN -> {
                addPreferencesFromResource(R.xml.settings_clock)
                addPreferencesFromResource(R.xml.settings_countdown)
            }
            else -> {}
        }

        // add another xml
        //			addPreferencesFromResource(R.xml.settings_stopwatch);
    }

    /**
     * Ensure at least one lap display format checkbox is set.
     * (lap_format_elapsed, lap_format_delta, lap_format_systime)
     * Fires when the user clicks to set or clear a checkbox.
     */
    override fun onSharedPreferenceChanged(settings: SharedPreferences, key: String) {

        if (key.startsWith(PREF_LAP_FORMAT_PREFIX)) {

            // read all 3 of the settings; default is LAP_FMT_FLAG_ELAPSED only
            val lapFmtElapsed = settings.getBoolean(PREF_LAP_FORMAT_ELAPSED, true)
            val lapFmtDelta = settings.getBoolean(PREF_LAP_FORMAT_DELTA, false)
            val lapFmtSystime = settings.getBoolean(PREF_LAP_FORMAT_SYSTIME, false)
            if (lapFmtElapsed || lapFmtDelta || lapFmtSystime)
                return

            val elapsed = findPreference(PREF_LAP_FORMAT_ELAPSED)
            if (elapsed != null && elapsed is CheckBoxPreference)
                elapsed.isChecked = true
            else
                preferenceScreen.sharedPreferences.edit().putBoolean(PREF_LAP_FORMAT_ELAPSED, true).apply()

            Toast.makeText(context, R.string.lap_display_format_must_select_one, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onResume() {
        super.onResume()
        preferenceScreen.sharedPreferences.registerOnSharedPreferenceChangeListener(this)
    }

    override fun onPause() {
        super.onPause()
        preferenceScreen.sharedPreferences.unregisterOnSharedPreferenceChangeListener(this)
    }
}