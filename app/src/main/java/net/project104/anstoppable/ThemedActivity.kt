/*
 * Copyright (c) 2018 by civyshk
 * civyshk@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.project104.anstoppable

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import net.project104.anstoppable.clock.ThemeManager

abstract class ThemedActivity(
        private val lightTheme: Int,
        private val darkTheme: Int
) : AppCompatActivity(), ThemeManager.Listener {

    private lateinit var themeManager: ThemeManager

    override fun onCreate(savedInstanceState: Bundle?) {
        logd("onCreate()")

        //First of all, load a working Theme.AppCompat to avoid issues with support action bar
        themeManager = (application as MyApp).themeManager
        setTheme(if (themeManager.isDarkTheme)
                    darkTheme
                else
                    lightTheme)

        super.onCreate(savedInstanceState)
        themeManager.addListener(this)

    }

    override fun onDestroy() {
        logd("onDestroy()")
        themeManager.removeListener(this)
        super.onDestroy()
    }

    override fun themeChanged() {
        logd("themeChanged()")
        recreate()

//        finish()
//        startActivity(Intent(this, ActivityMain::class.java))
    }
}