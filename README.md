# Anstoppable

Powerful counter & stopwatch for Android, GPL.

Credits and thanks to [magnusja](https://github.com/magnusja) and [jdmonin](https://github.com/jdmonin) because the code for clocks was taken from [anstop](https://github.com/jdmonin/anstop) and was heavily modified for this project.

# Download

Check screenshots and download app from [Google Play](https://play.google.com/store/apps/details?id=net.project104.anstoppable).

# Privacy

This app does not access, read, analyze nor collect any personal or non personal information.
